const axios = require('axios');
var tokenService = require ('../../utils/token');

export const authInstance = axios.create({
    baseURL: `${process.env.REACT_APP_SERVER}/auth`,
    withCredentials: true,
    credentials: 'same-origin',
    timeout: 15000,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
});

authInstance.interceptors.request.use(function (config) {    
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

// Add a response interceptor
authInstance.interceptors.response.use(function (response) {
  return response.status === 401 ? tokenService.clearToken() : response  
  }, function (error) {
    return error.response.status === 401 ? tokenService.clearToken() : Promise.reject(error)
  });

export const instance = axios.create({
    baseURL: `${process.env.REACT_APP_SERVER}`,
    withCredentials: true,
    credentials: 'same-origin',
    timeout: 15000,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },    
});

instance.interceptors.request.use(function (config) {
    const token = tokenService.getToken();
    config.headers.Authorization = `Bearer ${token}`;
    return config;
  }, function (error) {
    return Promise.reject(error);
});

// Add a response interceptor
instance.interceptors.response.use(function (response) {
  return response.status === 401 ? tokenService.clearToken() : response  
  }, function (error) {
    return error.response.status === 401 ? tokenService.clearToken() : Promise.reject(error)
});

export const hasuraInstance = axios.create({
  baseURL: `${process.env.REACT_APP_HASURA_ENDPOINT}`,
  withCredentials: true,
  credentials: 'same-origin',
  timeout: 15000,
  method: 'post',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },    
});

hasuraInstance.interceptors.request.use(function (config) {
  const token = tokenService.getToken();
  config.headers.Authorization = `Bearer ${token}`;
  return config;
}, function (error) {
  return Promise.reject(error);
});

// Add a response interceptor
hasuraInstance.interceptors.response.use(function (response) {
return response.status === 401 ? tokenService.clearToken() : response  
}, function (error) {
  return error.response.status === 401 ? tokenService.clearToken() : Promise.reject(error)
});