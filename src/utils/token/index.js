
import Cookies from 'js-cookie';

export const getToken = () => Cookies.get("access-token")
export const clearToken = () => Cookies.remove("access-token")
export const setCookie = (token) => Cookies.set("access-token", token)
export const getSplitToken = () => getToken()? getToken().split(".") : null
export const getTokenPayload = () => getSplitToken() ? getSplitToken()[1] : null
export const getTranslatedTokenString = () => getTokenPayload() ? atob(getTokenPayload()) : null
export const getTranslatedTokenAsJson = () => getTranslatedTokenString() ? JSON.parse(getTranslatedTokenString()) : null
export const getName = () => getTranslatedTokenAsJson() ? getTranslatedTokenAsJson().name : null
export const getEmail = () => getTranslatedTokenAsJson() ? getTranslatedTokenAsJson().email : null
export const getAdmin = () => getTranslatedTokenAsJson() ? getTranslatedTokenAsJson().role === 'admin' ? true : null : null
export const getRole = () => getTranslatedTokenAsJson() ? getTranslatedTokenAsJson().role : null
export const getExp = () => getTranslatedTokenAsJson() ? getTranslatedTokenAsJson().exp : null