export const handleChangeField = (setFieldValue, field, val) => {
    setFieldValue(field, val);
}