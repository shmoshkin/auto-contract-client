import React from "react";
import { Redirect, Route } from "react-router-dom";
import {LOGIN} from '../../constants/route';
import {getToken} from '../../utils/token';

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props => 
            getToken() ? (
            <Component {...props} client={rest.client}/>
            ) : (
                <Redirect
                    to={{
                        pathname: LOGIN,
                        state: {from: props.location}
                    }}
                />                
            )
        }
    />  
)

export default PrivateRoute;