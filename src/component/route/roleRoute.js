import React from "react";
import { Redirect, Route } from "react-router-dom";
import { HOME } from '../../constants/route';
import { getAdmin, getRole } from '../../utils/token';

const PrivateRoute = ({component: Component,role, ...rest}) => (
    <Route
        {...rest}
        render={props => 
            (getAdmin() || getRole() === role) ? (
            <Component {...props}/>
            ) : (
                <Redirect
                    to={{
                        pathname: HOME,
                        state: {from: props.location}
                    }}
                />                
            )
        }
    />  
)

export default PrivateRoute;