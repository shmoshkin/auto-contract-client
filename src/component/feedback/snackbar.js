import Snackbar from '@material-ui/core/Snackbar';
import React from 'react';

const DynamicSnackbar = (props) => {

  const { vertical = 'bottom', horizontal = 'right', message, open, setOpen } = props;

  return (
    <div>      
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        key={`${vertical},${horizontal}`}
        open={open}
        onClose={() => setOpen(false)}
        message={message}
        autoHideDuration={5000}
      />
    </div>
  );
}

export default DynamicSnackbar;