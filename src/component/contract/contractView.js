import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import DoneIcon from '@material-ui/icons/Done';
import EditIcon from '@material-ui/icons/Edit';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import GetAppIcon from '@material-ui/icons/GetApp';
import TranslateIcon from '@material-ui/icons/Translate';
import to from 'await-to-js';
import clsx from 'clsx';
import React, { useState } from 'react';
import { APPARTMENT, CAR, CONFIDENTIALITY, PARTNERSHIP } from '../../constants/route';
import { instance } from '../../rest/axios';
import { getRole } from '../../utils/token';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    direction: "rtl",
    textAlign: "center"
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

const ContractView = ({ contract, isEdit, isTranslate, isCancel, isApprove, onApprove, onCancel, isComment, history, isFromLawyer }) => {
  const classes = useStyles();
  const [expanded, setExpanded] = useState(false);

  const getTranslatedContractContent = async () => {
    const [err, res] = await to(instance.get(`/contract/${contract.type}/translate/${contract.contract_id}`))

    if (err) {
      console.log(err)
    } else {
      return res.data.data;
    }
  }

  const getContractContent = async () => {
    const [err, res] = await to(instance.get(`/contract/${contract.type}/download/${contract.contract_id}`))
    if (err) {
    } else {

      return res.data.data
    }
  }

  const downloadTxtFile = async () => {
    const content = await getContractContent()
    const element = document.createElement("a");
    const file = new Blob([content], { type: 'text/plain' });
    element.href = URL.createObjectURL(file);
    element.download = `${fileName}`;
    document.body.appendChild(element);
    element.click();
  }

  const downloadTranslatedTxtFile = async () => {
    const content = await getTranslatedContractContent()
    const element = document.createElement("a");
    const file = new Blob([content], { type: 'text/plain' });
    element.href = URL.createObjectURL(file);
    element.download = `${translatedFileName}`;
    document.body.appendChild(element);
    element.click();
  }

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  let cardMedia = <CardMedia
    component="img"
    alt="contract image"
    height="100"
    image={require("../../assets/images/car-1209912_1920.jpg")}
    pathname={CAR}
  />;

  switch (contract.type) {
    case "APP_RENTAL":
      cardMedia = <CardMedia
        component="img"
        alt="contract image"
        height="100"
        image={require("../../assets/images/new-1572747_1920.jpg")}
        pathname={APPARTMENT}
      />;
      break;
    case "PARTNERSHIP_CONTRACT":
      cardMedia = <CardMedia
        component="img"
        alt="contract image"
        height="100"
        image={require("../../assets/images/shaking-hands-3091908_1920.jpg")}
        pathname={PARTNERSHIP}
      />;
      break;
    case "SECRET_CONTRACT":
      cardMedia = <CardMedia
        component="img"
        alt="contract image"
        height="100"
        image={require("../../assets/images/security-2168233_1920.jpg")}
        pathname={CONFIDENTIALITY}
      />;
      break;
    default:
  }

  const fileName = `${contract.title}.txt`
  const translatedFileName = `${contract.title}-מתורגם.txt`

  return (
    <Card className={classes.root}>
      <CardActionArea>
        {cardMedia}
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {contract.title}
          </Typography>
          <Typography gutterBottom variant="body1" component="h2">
            <span style={{ fontWeight: "bolder" }}>סטטוס: </span>{contract.status}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        {
          contract.status === "מאושר" || (getRole() == 'admin' || (contract.status === "מחכה לאישור") && (getRole() == 'lawyer')) ?
            <Tooltip title="הורדה" placement="bottom">
              <IconButton onClick={downloadTxtFile}>
                <GetAppIcon />
              </IconButton>
            </Tooltip> : null
        }

        {isTranslate && contract.status === "מאושר" ?
          <Tooltip title="תרגום" placement="bottom">
            <IconButton onClick={downloadTranslatedTxtFile}>
              < TranslateIcon/>
            </IconButton>
          </Tooltip> : null}
        {isEdit ? <Tooltip title="עריכה" placement="bottom">
          <IconButton onClick={() => history.push({
            pathname: cardMedia.props.pathname,
            isUpdate: true,
            contractId: contract.contract_id,
            contract: contract,
            isFromLawyer
          })}>
            <EditIcon />
          </IconButton>
        </Tooltip> : null}
        {isCancel ? <Tooltip title="לא מאושר" placement="bottom">
          <IconButton onClick={() => onCancel(contract)}>
            <CloseIcon />
          </IconButton>
        </Tooltip>
          : null}
        {isApprove ? <Tooltip title="מאושר" placement="bottom">
          <IconButton onClick={() => onApprove(contract)}>
            <DoneIcon />
          </IconButton>
        </Tooltip>
          : null}
        {isComment && (contract.status === "לא מאושר" || contract.status === "מאושר") ? <Tooltip title="הערות" placement="bottom">
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </Tooltip>
          : null}

      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>הערות:</Typography>
          <Typography paragraph>
            {contract.lawyer_description}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

export default ContractView;