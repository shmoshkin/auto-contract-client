import DateFnsUtils from '@date-io/date-fns';
import { Button } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Field } from 'formik';
import React from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flip: false,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '1em',
    },
    rightColumn: {
        marginLeft: '2em',
        flip: false,
    },
    leftColumn: {
        marginRight: '2em',
        flip: false,
    },
    date: {
        width: '11.4em',
        marginRight: '2em',
        flip: false,
    },
    container: {
        textAlign: "center"
    },
    buttons: {
        marginTop: "25px"
    }
}));

const ApartmentLeaseStep1 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched, validateField }) => {
    const classes = useStyles();
    
    return (
        <div noValidate className={classes.container}>
            <FormLabel component="legend" className={classes.title}>כמה פרטים על העסקה וממשיכים..</FormLabel>
            <div className={classes.body}>
                <div className={classes.row}>
                    <Field
                        as={TextField}
                        required
                        name="place_of_sign"
                        label="מקום העסקה"
                        className={classes.rightColumn}
                        style={{ marginTop: '1em' }}
                        // error={errors["place_of_sign"] ? true : false}
                    />
                    {/* <ErrorMessage name="place_of_sign"/> */}

                    <div className={classes.date}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Field
                                required
                                as={KeyboardDatePicker}
                                disableToolbar
                                variant="inline"
                                format="dd/MM/yyyy"
                                margin="normal"
                                name="date_of_sign"
                                label="תאריך העסקה"
                                value={values["date_of_sign"] ? values["date_of_sign"] : null}
                                onChange={(date) => handleChangeField(setFieldValue, "date_of_sign", date)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                </div>
                <div className={classes.row}>
                    <Field
                        as={TextField}
                        required
                        name="owner_name"
                        label="שם המשכיר"
                        className={classes.rightColumn}
                    />
                    <Field
                        as={TextField}
                        required
                        name="owner_id"
                        type="number"
                        label="תז המשכיר"
                        className={classes.leftColumn}
                    />
                </div>
                <div className={classes.row}>
                    <Field
                        as={TextField}
                        required
                        name="renter_name"
                        label="שם השוכר"
                        className={classes.rightColumn}
                    />
                    <Field
                        as={TextField}
                        required
                        type="number"
                        name="renter_id"
                        label="תז השוכר"
                        className={classes.leftColumn}
                    />
                </div>
            </div>
            <div className={classes.buttons}>
                <Button
                    onClick={handleBack}
                >
                    הקודם
                    </Button>
                <Button onClick={() => handleNext(["place_of_sign", "date_of_sign", "owner_name", "owner_id", "renter_name", "renter_id"], setFieldTouched, errors)} variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
        </div>
    );
}

export default ApartmentLeaseStep1;