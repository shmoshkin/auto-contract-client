import { Button, Grid } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
  title: {
    marginTop: "2%",
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontSize: '2em',
    marginBottom: '1em',
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flip: false,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '1em',
    marginRight: '-11em',
    flip: false,
  },
  radioLabel: {
    marginTop: '-0.7em',
  },
  radioGroup: {
    marginRight: '4em',
    flip: false,
    display: 'flex',
    flexDirection: 'row',
    width: '25em',
  },
  container: {
    textAlign: "center"
  },
  buttons: {
    marginTop: "25px"
  },
  rightGrid: {
    marginRight: "14em",
    flip: false,
    marginTop: "2.4em"
  },
  leftGrid: {
    marginTop: "2em",
    marginRight: "-4em",
    flip: false
  }
}));

const ApartmentLeaseStep5 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
  const classes = useStyles();

  return (
    <div noValidate className={classes.container}>
      <FormLabel component="legend" className={classes.title}>מי משלם מה?</FormLabel>
      <Grid container>
        <Grid item xs={4} className={classes.rightGrid}>
          <FormLabel required component="legend">חשמל</FormLabel>
        </Grid>
        <Grid item xs={6} className={classes.leftGrid}>
          <RadioGroup name="electricity" value={values.electricity ? values.electricity : null} className={classes.radioGroup} onChange={(evt) => handleChangeField(setFieldValue, "electricity", evt.target.value)}>
            <FormControlLabel value="tenant" name="electricity" control={<Radio />} label="המשכיר" className={classes.radioLabel} />
            <FormControlLabel value="renter" name="electricity" control={<Radio />} label="השוכר" className={classes.radioLabel} />
          </RadioGroup>
        </Grid>
        <Grid item xs={4} className={classes.rightGrid}>
          <FormLabel required component="legend">מים</FormLabel>
        </Grid>
        <Grid item xs={6} className={classes.leftGrid}>
          <RadioGroup name="water" value={values.water ? values.water : null} className={classes.radioGroup} onChange={(evt) => handleChangeField(setFieldValue, "water", evt.target.value)}>
            <FormControlLabel value="tenant" name="water" control={<Radio />} label="המשכיר" className={classes.radioLabel} />
            <FormControlLabel value="renter" name="water" control={<Radio />} label="השוכר" className={classes.radioLabel} />
          </RadioGroup>
        </Grid>
        <Grid item xs={4} className={classes.rightGrid}>
          <FormLabel required component="legend">ארנונה</FormLabel>
        </Grid>
        <Grid item xs={6} className={classes.leftGrid}>
          <RadioGroup name="arnona" value={values.arnona ? values.arnona : null} className={classes.radioGroup} onChange={(evt) => handleChangeField(setFieldValue, "arnona", evt.target.value)}>
            <FormControlLabel value="tenant" control={<Radio />} label="המשכיר" className={classes.radioLabel} />
            <FormControlLabel value="renter" control={<Radio />} label="השוכר" className={classes.radioLabel} />
          </RadioGroup>
        </Grid>
        <Grid item xs={4} className={classes.rightGrid}>
          <FormLabel required component="legend">כבלים</FormLabel>
        </Grid>
        <Grid item xs={6} className={classes.leftGrid}>
          <RadioGroup name="tv" value={values.tv ? values.tv : null} className={classes.radioGroup} onChange={(evt) => handleChangeField(setFieldValue, "tv", evt.target.value)}>
            <FormControlLabel value="tenant" control={<Radio />} label="המשכיר" className={classes.radioLabel} />
            <FormControlLabel value="renter" control={<Radio />} label="השוכר" className={classes.radioLabel} />
          </RadioGroup>
        </Grid>
        <Grid item xs={4} className={classes.rightGrid}>
          <FormLabel required component="legend">אינטרנט</FormLabel>
        </Grid>
        <Grid item xs={6} className={classes.leftGrid}>
          <RadioGroup name="internet" value={values.internet ? values.internet : null} className={classes.radioGroup} onChange={(evt) => handleChangeField(setFieldValue, "internet", evt.target.value)}>
            <FormControlLabel value="tenant" control={<Radio />} label="המשכיר" className={classes.radioLabel} />
            <FormControlLabel value="renter" control={<Radio />} label="השוכר" className={classes.radioLabel} />
          </RadioGroup>
        </Grid>
        <Grid item xs={4} className={classes.rightGrid}>
          <FormLabel required component="legend">ועד בית</FormLabel>
        </Grid>
        <Grid item xs={6} className={classes.leftGrid}>
          <RadioGroup name="vaad" value={values.vaad ? values.vaad : null} className={classes.radioGroup} onChange={(evt) => handleChangeField(setFieldValue, "vaad", evt.target.value)}>
            <FormControlLabel value="tenant" control={<Radio />} label="המשכיר" className={classes.radioLabel} />
            <FormControlLabel value="renter" control={<Radio />} label="השוכר" className={classes.radioLabel} />
          </RadioGroup>
        </Grid>

      </Grid>
      <div className={classes.buttons}>
        <Button
          onClick={handleBack}
        >
          הקודם
            </Button>
        <Button onClick={() => handleNext(["electricity", "water", "arnona", "tv", "internet", "vaad"], setFieldTouched, errors)} variant="contained" color="primary">
          המשך
          </Button>
      </div>
    </div>
  );
}

export default ApartmentLeaseStep5;