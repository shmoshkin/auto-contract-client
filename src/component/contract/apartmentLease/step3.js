import { Button } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import IconButton from '@material-ui/core/IconButton';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutline from '@material-ui/icons/HelpOutline';
import { Field } from 'formik';
import React, { useState } from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'justify',
        flip: false,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
    },
    tooltip: {
        marginTop: '-0.7em',
    },
    radioLabel: {
        // width: '8em',
        marginTop: '1em',
        marginLeft: '14em',
        flip: false,
    },
    radioGroup: {
        display: 'contents',
        flexDirection: 'row',
        marginTop: '-0.5em',
    },
    rightFromCenter: {
        marginRight: '-25em',
        flip: false,
        marginTop: '5em',
    },
    container: {
        textAlign: "center"
    },
    buttons: {
        marginTop: "25px"
    },
    seperateTogether: {
        width: "46%"
    }
}));



const ApartmentLeaseStep3 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {

    const classes = useStyles();
    const [roomates, setRoomates] = useState([{ roomateName: "", roomateRoom: "", roomatePrice: 0 }]);
    const [tab, setTab] = useState(0);
    const [isPartners, setIsPartners] = useState();
    const [seperateTogether, setIsSeperateTogether] = useState();

    const addRoomate = () => {
        setRoomates(roomates.push([{ roomateName: "", roomateRoom: "", roomatePrice: 0 }]))
    }

    const handleChangeTab = (event, newValue) => {
        setTab(newValue);
    };

    const handleChangePartners = (evt) => {
        handleChangeField(setFieldValue, "is_partners_exists", evt.target.value);
        handleChangeField(setFieldValue, "is_partners_together", "no");
        setIsPartners(evt.target.value === "yes");
    }

    const handleChangeSeperateTogether = (evt) => {
        setIsSeperateTogether(evt.target.value === "yes")
        handleChangeField(setFieldValue, "is_partners_together", evt.target.value)
    }

    return (
        <div noValidate className={classes.container}>
            <FormLabel component="legend" className={classes.title}>נכנסים לדירה מספר שותפים?</FormLabel>
            <div className={classes.body}>
                <div className={classes.rightFromCenter}>
                    <div className={classes.row}>
                        <FormLabel required component="legend" className={classes.radioLabel}>האם יש שותפים בדירה?</FormLabel>
                        <RadioGroup className={classes.radioGroup} name="is_partners_exists" value={values["is_partners_exists"] ? values["is_partners_exists"] : null} onClick={handleChangePartners}>
                            <FormControlLabel value="yes" control={<Radio />} label="כן" />
                            <FormControlLabel value="no" control={<Radio />} label="לא" />
                        </RadioGroup>
                    </div>
                    {isPartners &&
                        <span>
                            <div className={classes.row}>
                                <FormLabel required className={classes.seperateTogether} component="legend">האם השותפים חייבים ביחד ולחוד?</FormLabel>
                                <Tooltip className={classes.tooltip} title="המשמעות של חייבים ביחד ולחוד היא שהמשכיר רשאי לגבות את שכר הדירה ממי שירצה מבין השותפים, ואף רשאי לגבות ממנו את כל הסכום">
                                    <IconButton aria-label="questionInfo">
                                        <HelpOutline />
                                    </IconButton>
                                </Tooltip>
                            </div>
                            <RadioGroup name="is_partners_together" value={values["is_partners_together"] ? values["is_partners_together"] : null} className={classes.radioGroup} onClick={handleChangeSeperateTogether}>
                                <FormControlLabel value="yes" control={<Radio />} label="כן" />
                                <FormControlLabel value="no" control={<Radio />} label="לא" />
                            </RadioGroup>
                        </span>
                    }
                    {isPartners && seperateTogether &&
                        <div>
                            <Field
                                as={TextField}
                                required
                                name="partners_details"
                                label="פירוט השותפים"
                                fullWidth
                            />
                        </div>
                    }
                </div>
            </div>
            <div className={classes.buttons}>
                <Button
                    onClick={handleBack}
                >
                    הקודם
                    </Button>
                <Button onClick={() => handleNext(["is_partners_together", "is_partners_exists"], setFieldTouched, errors)} variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
        </div>
    );
}

export default ApartmentLeaseStep3;