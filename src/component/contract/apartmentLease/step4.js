import DateFnsUtils from '@date-io/date-fns';
import { Button } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Field } from 'formik';
import React from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flip: false,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '1em',
    },
    rightColumn: {
        marginLeft: '2em',
        flip: false,
    },
    leftColumn: {
        marginRight: '2em',
        flip: false,
    },
    date: {
        width: '11.4em',
        marginRight: '2em',
        marginLeft: '2em',
        flip: false,
    },
    buttons: {
        marginTop: "25px"
    }
}));

const ApartmentLeaseStep3 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
    const classes = useStyles();

    return (
        <div noValidate className={classes.body}>
            <FormLabel component="legend" className={classes.title}>נדבר רגע על התשלום...</FormLabel>
            <div className={classes.row}>
                <div className={classes.date}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Field
                            required
                            as={KeyboardDatePicker}
                            format="dd/MM/yyyy"
                            margin="normal"
                            name="date_of_enter"
                            label="מתי נכנסים?"
                            value={values["date_of_enter"] ? values["date_of_enter"] : null}
                            onChange={(evt) => handleChangeField(setFieldValue, "date_of_enter", evt)}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </MuiPickersUtilsProvider>
                </div>
                <div className={classes.date}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Field
                            required
                            as={KeyboardDatePicker}
                            format="dd/MM/yyyy"
                            margin="normal"
                            name="date_of_leave"
                            label="מתי עוזבים?"
                            value={values["date_of_leave"] ? values["date_of_leave"] : null}
                            onChange={(evt) => handleChangeField(setFieldValue, "date_of_leave", evt)}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </MuiPickersUtilsProvider>
                </div>
            </div>
            <div className={classes.row}>
                <Field
                    as={TextField}
                    required
                    type="number"
                    name="price_for_month"
                    label="מחיר שכירות חודשית"
                    className={classes.rightColumn}
                />
                <Field
                    as={TextField}
                    required
                    type="number"
                    name="payment_period_in_months"
                    label="מספר תשלומים"
                    className={classes.leftColumn}
                />
            </div>
            <div className={classes.row}>
                <Field
                    as={TextField}
                    required
                    name="bail_name"
                    label="שם הערב לתשלום"
                    className={classes.rightColumn}
                />
                <Field
                    as={TextField}
                    required
                    name="bail_address"
                    label="כתובת הערב לתשלום"
                    className={classes.leftColumn}
                />
            </div>
            <div className={classes.buttons}>
                <Button
                    onClick={handleBack}
                >
                    הקודם
                    </Button>
                <Button onClick={() => handleNext(["date_of_enter", "date_of_leave", "price_for_month", "payment_period_in_months", "bail_name", "bail_address", "bail_address"], setFieldTouched, errors)} variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
        </div>
    );
}

export default ApartmentLeaseStep3;