import { Button } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Field } from 'formik';
import React from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        height: "90%",
        flip: false,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '2em',
    },
    apartmentRooms: {
        width: '8em',
        marginRight: '2.5em',
        flip: false,
    },
    apartmentDescription: {
        width: '30em',
        marginRight: '2em',
        flip: false,
    },
    furnitureDescription: {
        width: '53.5em',
        marginTop: '2em',
    },
    radioLabel: {
        width: '8em',
        marginTop: '2em',
        marginLeft: '14em',
        flip: false,
    },
    radioGroup: {
        marginTop: '3.5em',
        marginRight: '-11.2em',
        flip: false,
        display: 'flex',
        flexDirection: 'row',
    },
    container: {
        height: "100%",
        textAlign: "center"
    },
    buttons: {
        marginTop: "25px"
    }

}));

const ApartmentLeaseStep2 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
    const classes = useStyles();

    return (
        <div noValidate className={classes.container}>
            <FormLabel component="legend" className={classes.title}>ועכשיו על הדירה..</FormLabel>
            <div className={classes.body}>
                <div className={classes.row}>
                    <Field
                        as={TextField}
                        required
                        name="app_city"
                        label="עיר"
                        className={classes.apartmentRooms}
                    />
                    <Field
                        as={TextField}
                        required
                        name="app_street"
                        label="רחוב"
                        className={classes.apartmentRooms}
                    />
                    <Field
                        as={TextField}
                        required
                        name="app_num"
                        label="מספר בית"
                        type="number"
                        className={classes.apartmentRooms}
                    />
                    <Field
                        as={TextField}
                        type="number"
                        name="app_level"
                        label="קומה"
                        className={classes.apartmentRooms}
                    />
                    </div>
                    <div className={classes.row}>

                    <Field
                        as={TextField}
                        required
                        type="number"
                        name="app_rooms_number"
                        label="מספר החדרים"
                        className={classes.apartmentRooms}
                    />

                    <Field
                        as={TextField}
                        required
                        name="app_rooms_details"
                        label="פירוט החדרים"
                        className={classes.apartmentDescription}
                    />

                </div>
                <Field
                    as={TextField}
                    required
                    name="app_content_details"
                    label="פירוט הרהיטים"
                    className={classes.furnitureDescription}
                />

                <div className={classes.row}>
                    <FormLabel required component="legend" className={classes.radioLabel}>מותר חיות מחמד?</FormLabel>
                    <RadioGroup className={classes.radioGroup} name="is_pets_allowed" value={values["is_pets_allowed"] ? values["is_pets_allowed"] : null} onClick={(evt) => handleChangeField(setFieldValue, "is_pets_allowed", evt.target.value)}>
                        <FormControlLabel value="yes" control={<Radio />} label="כן" />
                        <FormControlLabel value="no" control={<Radio />} label="לא" />
                    </RadioGroup>
                    <FormLabel required component="legend" className={classes.radioLabel}>יש חניה?</FormLabel>
                    <RadioGroup name="is_parking_exists" className={classes.radioGroup} value={values["is_parking_exists"] ? values["is_parking_exists"] : null} onClick={(evt) => handleChangeField(setFieldValue, "is_parking_exists", evt.target.value)}>
                        <FormControlLabel value="yes" control={<Radio />} label="כן" />
                        <FormControlLabel value="no" control={<Radio />} label="לא" />
                    </RadioGroup>
                    <FormLabel required component="legend" className={classes.radioLabel}>יש מחסן?</FormLabel>
                    <RadioGroup name="is_warehouse_exists" className={classes.radioGroup} value={values["is_warehouse_exists"] ? values["is_warehouse_exists"] : null} onClick={(evt) => handleChangeField(setFieldValue, "is_warehouse_exists", evt.target.value)}>
                        <FormControlLabel value="yes" control={<Radio />} label="כן" />
                        <FormControlLabel value="no" control={<Radio />} label="לא" />
                    </RadioGroup>
                </div>
            </div>
            <div className={classes.buttons}>
                <Button
                    onClick={handleBack}
                >
                    הקודם
                    </Button>
                <Button onClick={() => handleNext(["app_city", "app_street", "app_num", "app_rooms_number", "app_rooms_details", "app_content_details", "is_pets_allowed", "is_parking_exists", "is_warehouse_exists"], setFieldTouched, errors)} type="submit" variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
        </div>
    );
}

export default ApartmentLeaseStep2;