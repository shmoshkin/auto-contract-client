import DateFnsUtils from '@date-io/date-fns';
import { Button } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Field } from 'formik';
import React from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flip: false,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '1em',
    },
    field: {
        marginRight: '1em',
        marginLeft: '1em',
    },
    date: {
        width: '11.4em',
        marginRight: '1em',
        marginLeft: '1em',
        marginTop: '-1em',
    },
    buttons: {
        textAlign: "center",
        marginTop: "20px"
    }
}));

const ConfidentialityStep1 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
    const classes = useStyles();

    return (
        <div>
            <FormLabel component="legend" className={classes.title}>כמה פרטים על חותמי החוזה..</FormLabel>
            <div className={classes.body}>
                <div className={classes.row}>
                    <div className={classes.date}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Field
                                required
                                as={KeyboardDatePicker}
                                disableToolbar
                                variant="inline"
                                format="dd/MM/yyyy"
                                margin="normal"
                                name="date_of_sign"
                                label="תאריך העסקה"
                                value={values["date_of_sign"] ? values["date_of_sign"] : null}
                                onChange={(date) => handleChangeField(setFieldValue, "date_of_sign", date)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                    <Field
                        as={TextField}
                        required
                        name="inventor_name"
                        label="שם הממציא"
                        className={classes.field}
                    />
                    <Field
                        as={TextField}
                        required
                        type="number"
                        name="inventor_id"
                        label="תז הממציא"
                        className={classes.field}
                    />
                </div>
                <div className={classes.row}>
                    <Field
                        as={TextField}
                        required
                        name="commiter_name"
                        label="שם המתחייב"
                        className={classes.field}
                    />

                    <Field
                        as={TextField}
                        required
                        name="commiter_id"
                        type="number"
                        label="תז המתחייב"
                        className={classes.field}
                    />

                    <Field
                        as={TextField}
                        required
                        name="commiter_city"
                        label="עיר המתחייב"
                        className={classes.field}
                    />
                </div>
            </div>
            <div className={classes.buttons}>
                <Button onClick={handleBack} >
                    הקודם
                </Button>
                <Button onClick={() => handleNext(["date_of_sign", "inventor_name", "inventor_id", "commiter_name", "commiter_id", "commiter_city"], setFieldTouched, errors)} variant="contained" color="primary">
                    המשך
                </Button>
            </div>
        </div>
    );
}

export default ConfidentialityStep1;