import { Button, Grid } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Field } from 'formik';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  title: {
    marginTop: "2%",
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontSize: '2em',
    marginBottom: '1em',
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flip: false,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '1em',
    justifyContent: "center"
  },
  field: {
    marginRight: '1em',
    marginLeft: '1em',
  },
  buttons: {
    textAlign: "center",
    marginTop: "20px"
  },
  comp: {
    marginLeft: "15px",
    flip: false
  }
}));

const ConfidentialityStep2 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
  const classes = useStyles();

  return (
    <div>
      <FormLabel component="legend" className={classes.title}>ועכשיו על ההמצאה..</FormLabel>
      <div className={classes.body}>
        <div className={classes.row}>
          <Field
            as={TextField}
            required
            name="invention_details"
            label="פירוט ההמצאה"
            className={classes.field}
            style={{ width: '40em' }}
            multiline
            variant="outlined"
            rows={3}
          />
        </div>
        <Grid container className={classes.row}>
          <Grid item xs={2} className={classes.comp}>
            <Field
              as={TextField}
              required
              name="violation_price"
              type="number"
              label="מחיר פיצוי מוסכם (בהפרה)"
              className={classes.field}
              fullWidth
            />
          </Grid>
          <Grid item xs={2}>
            <Field
              as={TextField}
              required
              name="city_of_courts"
              label="העיר בה יעסקו בתי המשפט"
              className={classes.field}
              fullWidth
            />
          </Grid>
        </Grid>
      </div>
      <div className={classes.buttons}>
        <Button onClick={handleBack}>
          הקודם
        </Button>
        <Button onClick={() => handleNext(["invention_details", "violation_price", "city_of_courts"], setFieldTouched, errors)} variant="contained" color="primary">
          המשך
        </Button>
      </div>
    </div>
  );
}

export default ConfidentialityStep2;