import DateFnsUtils from '@date-io/date-fns';
import { Button, Grid } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Field } from 'formik';
import React from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flip: false,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '1em',
    },
    field: {
        marginLeft: '1em',
        marginRight: '1em',
    },
    date: {
        width: '11.7em',
        marginRight: '6em',
        marginLeft: '4em',
        flip: false,
    },
    container: {
        textAlign: "right",
        flip: false
    },
    buttons: {
        textAlign: "center",
        marginTop: "20px"
    }
}));

const CarStep2 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <FormLabel component="legend" className={classes.title}>כמה פרטים על העסקה וממשיכים..</FormLabel>
            <div className={classes.body}>
                <div className={classes.row}>
                    <Grid>
                        <Field
                            required
                            as={TextField}
                            type="number"
                            name="car_id"
                            label="מספר רישוי"
                            className={classes.field}
                        />
                        {/* {touched["car_id"] && errors["car_id"] && <div>{errors["car_id"]}</div>} */}
                    </Grid>
                    <Grid>
                        <Field
                            required
                            as={TextField}
                            name="car_brand"
                            label="סוג הרכב"
                            className={classes.field}
                        />
                        {/* {touched["car_brand"] && errors["car_brand"] && <div>{errors["car_brand"]}</div>} */}
                    </Grid>
                    <Grid>
                        <Field
                            required
                            as={TextField}
                            name="car_year"
                            type="number"
                            label="שנת הרכב"
                            className={classes.field}
                        />
                        {/* {touched["car_year"] && errors["car_year"] && <div>{errors["car_year"]}</div>} */}
                    </Grid>
                </div>
                <div className={classes.row}>
                    <Grid>
                        <Field
                            required
                            as={TextField}
                            name="car_km"
                            label="קילומטראז"
                            type="number"
                            className={classes.field}
                            style={{ marginRight: '4em', marginLeft: '0em', width: '11.7em', flip: false }}
                        />
                        {/* {touched["car_km"] && errors["car_km"] && <div>{errors["car_km"]}</div>} */}
                    </Grid>
                    <FormLabel required component="legend" style={{ marginRight: '-1em', marginTop: '1.2em', width: '11.7em' }}>האם הרכב עבר תאונות?</FormLabel>
                    <RadioGroup name="car_is_hit" className={classes.radioGroup} value={values.car_is_hit} style={{ display: 'contents' }} onChange={(evt) => handleChangeField(setFieldValue, "car_is_hit", evt.target.value)}>
                        <FormControlLabel value="yes" control={<Radio />} label="כן" style={{ marginRight: '-10em', marginTop: '2em' }} />
                        <FormControlLabel value="no" control={<Radio />} label="לא" style={{ marginTop: '2em' }} />
                    </RadioGroup>
                    <div className={classes.date}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Field
                                as={KeyboardDatePicker}
                                required
                                format="dd/MM/yyyy"
                                margin="normal"
                                name="car_test_end"
                                label="מועד סיום טסט"
                                value={values["car_test_end"] ? values["car_test_end"] : null}
                                onChange={(evt) => handleChangeField(setFieldValue, "car_test_end", evt)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                </div>
            </div>
            <div className={classes.buttons}>
                <Button
                    onClick={handleBack}
                >
                    הקודם
                    </Button>
                <Button onClick={() => handleNext(["car_id", "car_brand", "car_year", "car_km", "car_is_hit", "car_test_end"], setFieldTouched, errors)} variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
        </div>
    );
}

export default CarStep2;