import DateFnsUtils from '@date-io/date-fns';
import { Button } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Field } from 'formik';
import React from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flip: false,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '1em',
    },
    date: {
        width: '11.4em',
    },
    buttons: {
        textAlign: "center",
        marginTop: "20px"
    }
}));

const ApartmentLeaseStep1 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
    const classes = useStyles();

    return (
        <div>
            <FormLabel component="legend" className={classes.title}>נדבר רגע על התשלום..</FormLabel>
            <div className={classes.body}>
                <div className={classes.row}>
                    <Field
                        required
                        as={TextField}
                        name="car_price"
                        label="מחיר הרכב (בשקלים)"
                        type="number"
                    />
                </div>
                <div className={classes.row}>
                    <div className={classes.date}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Field
                                disableToolbar
                                required
                                as={KeyboardDatePicker}
                                variant="inline"
                                format="dd/MM/yyyy"
                                margin="normal"
                                name="payment_date"
                                label="מתי משלמים?"
                                value={values["payment_date"] ? values["payment_date"] : null}
                                onChange={(evt) => handleChangeField(setFieldValue, "payment_date", evt)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                </div>
                <div className={classes.row}>
                    <div className={classes.date}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                required
                                format="dd/MM/yyyy"
                                margin="normal"
                                name="delivery_date"
                                label="מתי מקבלים את הרכב?"
                                value={values["delivery_date"] ? values["delivery_date"] : null}
                                onChange={(evt) => handleChangeField(setFieldValue, "delivery_date", evt)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                </div>
            </div>
            <div className={classes.buttons}>
                <Button
                    onClick={handleBack}
                >
                    הקודם
                    </Button>
                <Button onClick={() => handleNext(["car_price", "payment_date", "delivery_date"], setFieldTouched, errors)} variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
        </div>
    );
}

export default ApartmentLeaseStep1;