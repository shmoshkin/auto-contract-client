import DateFnsUtils from '@date-io/date-fns';
import { Button, Grid } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Field } from 'formik';
import React from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flip: false,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '1em',
    },
    rightColumn: {
        marginLeft: '2em',
        flip: false,
    },
    leftColumn: {
        // marginRight: '2em',
        flip: false,
    },
    date: {
        width: '11.4em',
        marginRight: '2em',
        flip: false,
    },
    container: {
        textAlign: "right",
        flip: false
    },
    buttons: {
        textAlign: "center",
        marginTop: "20px"
    }
}));

const CarStep1 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <FormLabel component="legend" className={classes.title}>כמה פרטים על העסקה וממשיכים..</FormLabel>
            <div className={classes.body}>
                <div className={classes.row}>
                    {/* {touched["place_of_sign"] && errors["place_of_sign"] && <ErrorTooltipIcon message={errors["place_of_sign"]} />} */}
                    <Grid>
                        <Field
                            required
                            as={TextField}
                            name="place_of_sign"
                            label="מקום העסקה"
                            className={classes.rightColumn}
                            style={{ marginTop: '1em' }}
                        />
                        {/* {touched["place_of_sign"] && errors["place_of_sign"] && <div>{errors["place_of_sign"]}</div>} */}
                    </Grid>

                    <div className={classes.date}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Field
                                disableToolbar
                                as={KeyboardDatePicker}
                                variant="inline"
                                format="dd/MM/yyyy"
                                margin="normal"
                                name="date_of_sign"
                                label="תאריך העסקה"
                                value={values["date_of_sign"] ? values["date_of_sign"] : null}
                                onChange={(evt) => handleChangeField(setFieldValue, "date_of_sign", evt)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                </div>
                <div className={classes.row}>
                    <Grid>
                        <Field
                            required
                            as={TextField}
                            name="owner_name"
                            label="שם המוכר"
                            className={classes.rightColumn}
                        />
                        {/* {touched["owner_name"] && errors["owner_name"] && <div>{errors["owner_name"]}</div>} */}
                    </Grid>
                    <Grid>
                        <Field
                            required
                            type="number"
                            as={TextField}
                            name="owner_id"
                            label="תז המוכר"
                            className={classes.leftColumn}
                        />
                        {/* {touched["owner_id"] && errors["owner_id"] && <div>{errors["owner_id"]}</div>} */}
                    </Grid>

                </div>
                <div className={classes.row}>
                    <Grid>
                        <Field
                            required
                            as={TextField}
                            name="buyer_name"
                            label="שם הקונה"
                            className={classes.rightColumn}
                        />
                        {/* {touched["buyer_name"] && errors["buyer_name"] && <div>{errors["buyer_name"]}</div>} */}
                    </Grid>
                    <Grid>
                        <Field
                            required
                            // classes={
                            //     root: classes.numberInput,
                            //     input: classes.numberInput
                            // }
                            type="number"
                            as={TextField}
                            name="buyer_id"
                            label="תז הקונה"
                            className={classes.leftColumn}
                        />
                        {/* {touched["buyer_id"] && errors["buyer_id"] && <div>{errors["buyer_id"]}</div>} */}
                    </Grid>
                </div>
            </div>
            <div className={classes.buttons}>
                <Button
                    onClick={handleBack}
                >
                    הקודם
                    </Button>
                <Button onClick={() => handleNext(["place_of_sign", "date_of_sign", "owner_name", "owner_id", "buyer_name", "buyer_id"], setFieldTouched, errors)} variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
        </div>
    );
}

export default CarStep1;