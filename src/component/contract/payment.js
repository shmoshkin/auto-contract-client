import { Button } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  title: {
    marginTop: "2%",
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontSize: '2em',
    marginBottom: '1em',
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flip: false,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '1em',
  },
  rightColumn: {
    marginLeft: '2em',
    flip: false,
  },
  leftColumn: {
    marginRight: '2em',
    flip: false,
  },
  date: {
    width: '11.4em',
    marginRight: '2em',
    flip: false,
  },
  buttons: {
    textAlign: "center",
    marginTop: "20px"
  }
}));

const Payment = ({ handleSubmit, handleBack }) => {
  const classes = useStyles();
  const [selectedDate, setSelectedDate] = React.useState(new Date());
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  return (
    <div>
      <FormLabel component="legend" className={classes.title}>תשלום</FormLabel>
      <div className={classes.body}>
        <div className={classes.row}>
          <TextField id="customer-id" label="תז בעל הכרטיס" className={classes.rightColumn} />
          <TextField id="card-number" label="מספר כרטיס האשראי" className={classes.leftColumn} />
        </div>
        <div className={classes.row}>
          <Select defaultValue="0" id="experation-year" className={classes.rightColumn} style={{ width: '4em' }}>
            <MenuItem hidden={true} value="0">שנה</MenuItem>
            <MenuItem value="2020">2020</MenuItem>
            <MenuItem value="2021">2021</MenuItem>
            <MenuItem value="2022">2022</MenuItem>
            <MenuItem value="2023">2023</MenuItem>
            <MenuItem value="2024">2024</MenuItem>
            <MenuItem value="2025">2025</MenuItem>
          </Select>
          <Select defaultValue="0" id="experation-month" className={classes.rightColumn} style={{ width: '4em' }}>
            <MenuItem hidden={true} value="0">חודש</MenuItem>
            <MenuItem value="1">1</MenuItem>
            <MenuItem value="2">2</MenuItem>
            <MenuItem value="3">3</MenuItem>
            <MenuItem value="4">4</MenuItem>
            <MenuItem value="5">5</MenuItem>
            <MenuItem value="6">6</MenuItem>
            <MenuItem value="7">7</MenuItem>
            <MenuItem value="8">8</MenuItem>
            <MenuItem value="9">9</MenuItem>
            <MenuItem value="10">10</MenuItem>
            <MenuItem value="11">11</MenuItem>
            <MenuItem value="12">12</MenuItem>
          </Select>
          <TextField id="cvv" label="קוד cvv" className={classes.leftColumn} style={{ width: '4em' }} />
        </div>
      </div>
      <div className={classes.buttons}>
        <Button
          onClick={handleBack}
        >
          הקודם
            </Button>
        <Button type="submit" variant="contained" color="primary">
          סיימתי!
          </Button>
      </div>
    </div>
  );
}

export default Payment;