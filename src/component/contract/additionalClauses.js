import { Button, Divider, IconButton, List, ListItem, ListItemText, TextField, Typography } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import { Close } from '@material-ui/icons';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { withStyles } from '@material-ui/styles';
import { Field } from 'formik';
import React, { Fragment, useState } from 'react';

const styles = theme => ({
  container: {
    textAlign: "center",
    width: "60%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  textField: {
    width: "65%"
  },
  listContainer: {
    width: "70%",
    marginRight: "12%",
    flip: false
  },
  closeIcon: {
    fontSize: "15px"
  },
  title: {
    marginTop: "2%",
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontSize: '2em',
    marginBottom: '1em',
  },
  title2: {
    textAlign: "center"
  },
  title3: {
    marginBottom: "2em",
    textAlign: "center"
  },
});
const AdditionalClauses = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, classes }) => {

  const [additionalClauses, setAdditionalClauses] = useState(values.additionalClauses ? values.additionalClauses : []);
  const [clauseText, setClauseText] = useState("");

  const handleAddClause = (event) => {

    if (clauseText !== "" && clauseText !== undefined) {
      setAdditionalClauses(
        [...additionalClauses, {text: clauseText, id: null}]
      )
      setFieldValue("additionalClauses", [...additionalClauses, {text: clauseText, id: null}])
      setClauseText("");
    }
  }

  const handleDeleteClause = (index) => {
    let clausesAfterDelete = [...additionalClauses.slice(0, index), ...additionalClauses.slice(index + 1)];
    setAdditionalClauses(clausesAfterDelete);
    setFieldValue("additionalClauses", clausesAfterDelete);
  }

  const handleChangeInput = (event) => {
    const text = event.target.value;
    setClauseText(text)
  }


  return (
    <div className={classes.container}>
      <FormLabel component="legend" className={classes.title}>בא לך להוסיף סעיפים נוספים?</FormLabel>
      <Typography className={classes.title2} variant="body2">לאחר הוספת הסעיפים, הם יעברו לבדיקת עורך דין כדי לוודא שהם לא סותרים חלקים בחוזה.</Typography>
      <Typography className={classes.title3} variant="body2">תקבלו עדכון במידה והם יאושר או במידה ויש לבצע שינוי</Typography>

      <Field
        as={TextField}
        fullWidth
        id="clauseDescription"
        label="תוכן הסעיף"
        onChange={handleChangeInput}
        value={clauseText}
        multiline
        variant="outlined"
        rows="2"
        classes={{
          root: classes.textField
        }}
      />
      <IconButton aria-label="AddCircleIcon" color="primary" onClick={handleAddClause} value={clauseText}>
        <AddCircleIcon />
      </IconButton>

      <List className={classes.listContainer}>
        {additionalClauses.map((clause, index) => {
          return (<Fragment key={index} >
            <ListItem>
              <IconButton onClick={() => handleDeleteClause(index)}>
                <Close className={classes.closeIcon} />
              </IconButton>
              <ListItemText primary={additionalClauses[index].text} />
            </ListItem>
            <Divider />
          </Fragment>)
        })}
      </List>
      <div className={classes.buttons}>
        <Button onClick={handleBack}>
          הקודם
        </Button>
        <Button onClick={() => handleNext([], null, errors)} variant="contained" color="primary">
          המשך
        </Button>
      </div>
    </div>
  );
}

export default withStyles(styles)(AdditionalClauses);