
// // Create styles
// const styles = StyleSheet.create({
//   page: {
//     flexDirection: 'row',
//     direction: 'rtl',
//     backgroundColor: '#E4E4E4',
//     fontFamily: 'Rubik',
//     textAlign: 'center',
//   },
//   section: {
//     margin: 10,
//     padding: 10,
//     flexGrow: 1,
//     fontFamily: 'Rubik',
//     width: 200
//   }
// });



import { Document, Font, Page, StyleSheet, Text, View } from '@react-pdf/renderer';
import React from 'react';
import font from './font/Rubik-Regular.ttf';
Font.register({
  family: 'Rubik',
  src: font
});
// Create styles
const styles = StyleSheet.create({
  body: { direction: 'rtl'},
  page: {
    // flexDirection: 'row',
    backgroundColor: '#E4E4E4',
    fontFamily: 'Rubik',
    direction: 'rtl',
    textAlign: 'center',
    fontSize: 8,
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
    fontFamily: 'Rubik',
    direction: 'rtl',
    textAlign: 'center',
    fontSize: 8,
  }
});


export const TranslatedContractPdf = ({text}) => (
  <Document>
    <Page size="A4" style={styles.page}>
      <View style={styles.section}>
        <Text>{text}</Text>
      </View>
      <View style={styles.section}>
        <Text>Section #2</Text>
      </View>
    </Page>
  </Document>
);


// Create Document Component
const MyDocument = ({content}) => (
  <Document direction='rtl'>
    <Page size="A4" style={styles.page}>      
      <View style={styles.section}>
        <Text>{content}</Text>
      </View>
    </Page>
  </Document>
);

export default MyDocument;
