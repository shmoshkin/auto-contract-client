/* eslint-disable */

import { Button, ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { withStyles } from '@material-ui/styles';
import React, { useEffect, useState } from 'react';
import { useQuery } from 'react-apollo';
import { QUERY_GET_SUGGESTED_CLAUSES } from '../../graphql/queries/contracts';

const styles = theme => ({
    root: {
        width: '70%',
        marginLeft: "auto",
        marginRight: "auto"
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
    },
    title2: {
        textAlign: "center"
    },
    title3: {
        marginBottom: "2em",
        textAlign: "center"
    },
    buttons: {
        textAlign: "center",
        marginTop: "20px"
    },
    loading: {
        textAlign: "center"
    }
});

const SuggestedClauses = ({ classes, type, handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, getSimilarData }) => {

    const [selectedClauses, setSelectedClauses] = useState(values.suggestedClauses ? values.suggestedClauses : {});
    const [tagGroups, setTagGroups] = useState({});
    const [isGotData, setIsGotData] = useState(false);
    const {
        data,
        loading,
        error
    } = useQuery(QUERY_GET_SUGGESTED_CLAUSES, {
        variables: { contractType: type },
    });

    useEffect(() => {
        setFieldValue("suggestedClauses", selectedClauses)
    }, [selectedClauses])

    useEffect(() => {

        async function handleGroups() {
            const similarData = await getSimilarData(values)
            if (similarData) {
                // Group the clauses by it's tags
                const groups = similarData.reduce((r, a) => {
                    r[a.tag] = [...r[a.tag] || [], a];
                    return r;
                }, {});



                let initialSelectedClauses = {};

                Object.keys(groups).forEach(key => {
                    initialSelectedClauses[key] = 0
                });

                if (values.suggestedClauses) {
                    Object.keys(values["suggestedClauses"]).forEach(key => {
                        initialSelectedClauses[key] = values.suggestedClauses[key];
                    })
                }

                setTagGroups(groups);
                setSelectedClauses(initialSelectedClauses);
                setIsGotData(true);
            }
        }
        if (type === "CAR_RENTAL" || type === "APP_RENTAL") {
            handleGroups();
        }

    }, [])

    useEffect(() => {
        if ((type === "PARTNERSHIP_CONTRACT" || type === "SECRET_CONTRACT") && data) {
            // Group the clauses by it's tags
            const groups = data.CLAUSES_FOR_SUGGEST.reduce((r, a) => {
                r[a.tag] = [...r[a.tag] || [], a];
                return r;
            }, {});



            let initialSelectedClauses = {};

            Object.keys(groups).forEach(key => {
                initialSelectedClauses[key] = 0
            });

            if (values.suggestedClauses) {
                Object.keys(values["suggestedClauses"]).forEach(key => {
                    initialSelectedClauses[key] = values.suggestedClauses[key];
                })
            }

            setTagGroups(groups);
            setSelectedClauses(initialSelectedClauses);
            setIsGotData(true);
        }
    }, [data])

    // if (loading) {
    //     return <div className={classes.loading}>טוען את הנתונים...</div>
    // }
    // if (error) {
    //     return <div>שגיאה! {error.message}</div>
    // }

    const handleBeforeNextPage = (errors) => {
        const tempSelectedClauses = selectedClauses;
        Object.keys(tempSelectedClauses).forEach(clause => {
            if (tempSelectedClauses[clause] === 0) {
                delete tempSelectedClauses[clause];
            }
        })
        setFieldValue("suggestedClauses", tempSelectedClauses);
        handleNext([], null, errors);
    }

    // const [tags, setTags] = useState(getTags());
    const handeChangeRadio = (event, key) => {
        const chosen = Number(event.target.value);
        setSelectedClauses({
            ...selectedClauses,
            [key]: chosen
        })
    };


    return (
        <span>
            {Object.keys(tagGroups).length !== 0 && isGotData && <div className={classes.root}>
                <FormLabel component="legend" className={classes.title}>יש סעיפים נוספים שמדברים אליך?</FormLabel>
                <Typography className={classes.title2} variant="body2">שימו לב! הסעיפים מחולקים לפי קטגוריות ככה שניתן לבחור סעיף אחד מכל קטגוריה.</Typography>
                <Typography className={classes.title3} variant="body2"> עשינו את זה כדי למנוע התנגשות בין סעיפים בחוזה שלכם :)</Typography>
                {Object.keys(tagGroups).map(key => {
                    return <ExpansionPanel key={key}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography className={classes.heading}>{key}</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            {Object.keys(selectedClauses).length !== 0 && <RadioGroup aria-label="gender" value={selectedClauses[key]} name="gender1" onChange={(val) => handeChangeRadio(val, key)}>
                                <FormControlLabel value={0} control={<Radio />} label="לא מעוניין להוסיף מקבוצת הסעיפים" />
                                {tagGroups[key].map((clause, index) => {
                                    return <FormControlLabel key={`${clause.id}-${index}`} value={clause.id} control={<Radio />} label={clause.text} />
                                })}
                            </RadioGroup>}
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                })}
            </div >}
                {!isGotData && <div className={classes.loading}>טוען את הנתונים...</div>}
                {isGotData && Object.keys(tagGroups).length === 0 && <div className={classes.loading}>אין לנו סעיפים להציע לך כרגע...</div>}
            <div className={classes.buttons}>
                <Button onClick={handleBack}>
                    הקודם
                </Button>
                <Button onClick={() => handleBeforeNextPage(errors)} variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
        </span>
    );
}

export default withStyles(styles)(SuggestedClauses);