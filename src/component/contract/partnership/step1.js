import { Button } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Field } from 'formik';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  title: {
    marginTop: "2%",
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontSize: '2em',
    marginBottom: '1em',
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flip: false,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '2em',
  },
  field: {
    marginRight: '1em',
    marginLeft: '1em',
  },
  buttons: {
    textAlign: "center",
    marginTop: "20px"
}
}));

const PartnershipStep2 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
  const classes = useStyles();

  return (
    <div>
      <FormLabel component="legend" className={classes.title}>כמה פרטים על השותפים וממשיכים..</FormLabel>
      <div className={classes.body}>
        <div className={classes.row}>
          <Field
            as={TextField}
            required
            name="a_person_name"
            label="שם צד א'"
            className={classes.field}
          />
          <Field
            as={TextField}
            required
            type="number"
            name="a_person_id"
            label="תז צד א'"
            className={classes.field}
          />
          <Field
            as={TextField}
            required
            name="a_person_address"
            label="כתובת צד א'"
            className={classes.field}
          />
        </div>
        <div className={classes.row}>
          <Field
            as={TextField}
            required
            name="b_person_name"
            label="שם צד ב'"
            className={classes.field}
          />
          <Field
            as={TextField}
            required
            type="number"
            name="b_person_id"
            label="תז צד ב'"
            className={classes.field}
          />
          <Field
            as={TextField}
            required
            name="b_person_address"
            label="כתובת צד ב'"
            className={classes.field}
          />
        </div>
      </div>
      <div className={classes.buttons}>
                <Button
                    onClick={handleBack}
                >
                    הקודם
                    </Button>
                <Button onClick={() => handleNext(["a_person_name", "a_person_id", "a_person_address", "b_person_name", "b_person_id", "b_person_address"], setFieldTouched, errors)} variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
    </div>
  );
}

export default PartnershipStep2;