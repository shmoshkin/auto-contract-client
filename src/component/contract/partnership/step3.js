import DateFnsUtils from '@date-io/date-fns';
import { Button } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutline from '@material-ui/icons/HelpOutline';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Field } from 'formik';
import React from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flip: false,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '1em',
    },
    date: {
        width: '11.4em',
        marginRight: '2em',
        flip: false,
    },
    field: {
        marginRight: '1em',
        marginLeft: '1em',
        marginTop: '1em',
    },
    buttons: {
        textAlign: "center",
        marginTop: "20px"
    }
}));

const PartnershipStep3 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
    const classes = useStyles();

    return (
        <div>
            <FormLabel component="legend" className={classes.title}>עוד כמה פרטים נוספים..</FormLabel>
            <div className={classes.body}>
                <div className={classes.row}>
                    <Field
                        as={TextField}
                        required
                        name="a_person_percents"
                        label="חלקו של צד א' (באחוזים)"
                        type="number"
                        className={classes.field}
                        style={{ width: '12em' }}
                    />
                    <Field
                        as={TextField}
                        required
                        name="a_investment"
                        label="ההון ההתחלתי שישקיע צד א'"
                        type="number"
                        className={classes.field}
                        style={{ width: '14em' }}
                    />
                    <Field
                        as={TextField}
                        required
                        name="a_person_job"
                        label="עיסוקו של צד א'"
                        className={classes.field}
                        style={{ width: '16em' }}
                    />
                </div>
                <div className={classes.row}>
                    <Field
                        as={TextField}
                        required
                        name="b_person_percents"
                        label="חלקו של צד ב' (באחוזים)"
                        type="number"
                        className={classes.field} style={{ width: '12em' }}
                    />
                    <Field
                        as={TextField}
                        required
                        name="b_investment"
                        label="ההון ההתחלתי שישקיע צד ב'"
                        type="number"
                        className={classes.field} style={{ width: '14em' }}
                    />
                    <Field
                        as={TextField}
                        required
                        name="b_person_job"
                        label="עיסוקו של צד ב'"
                        className={classes.field} style={{ width: '16em' }}
                    />
                </div>
                <div className={classes.row}>
                    <div className={classes.field} style={{ marginTop: '0em', width: '9em' }}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Field
                                required
                                as={KeyboardDatePicker}
                                disableToolbar
                                variant="inline"
                                format="dd/MM/yyyy"
                                margin="normal"
                                name="end_investment_date"
                                label="תאריך תום השקעה"
                                value={values["end_investment_date"] ? values["end_investment_date"] : null}
                                onChange={(date) => handleChangeField(setFieldValue, "end_investment_date", date)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                    <Field
                        as={TextField}
                        required
                        name="details_on_calc_books"
                        label="עניינים שירשמו בספרי חשבונות"
                        className={classes.field} style={{ width: '20em' }}
                    />
                    <Field
                        as={TextField}
                        required
                        name="court_for_contract"
                        label="בית המשפט הרשאי לעסוק"
                        className={classes.field} style={{ width: '13em' }}
                    />
                </div>
                <div className={classes.row}>
                    <Tooltip className={classes.tooltip} title="המשמעות של סעיף זה היא מספר השנים שאסור לשותף לפתוח עסק מתחרה בעסקי השותפות, במידה ופרש מהסכם השותפות">
                        <IconButton aria-label="questionInfo">
                            <HelpOutline />
                        </IconButton>
                    </Tooltip>
                    <Field
                        as={TextField}
                        required
                        type="number"
                        name="years_of_no_competition"
                        label="זמן אי עיסוק בעסק מתחרה (בשנים)"
                        className={classes.field} style={{ width: '20em' }}
                    />
                </div>

            </div>
            <div className={classes.buttons}>
                <Button
                    onClick={handleBack}
                >
                    הקודם
                    </Button>
                <Button onClick={() => handleNext(["a_person_percents", "a_investment", "a_person_job", "b_person_percents", "b_investment", "b_person_job", "end_investment_date", "details_on_calc_books", "years_of_no_competition", "court_for_contract"], setFieldTouched, errors)} variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
        </div>
    );
}

export default PartnershipStep3;