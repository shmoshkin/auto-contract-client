import DateFnsUtils from '@date-io/date-fns';
import { Button } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Field } from 'formik';
import React from 'react';
import { handleChangeField } from '../../../utils/contract';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flip: false,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '1em',
    },
    date: {
        width: '11.4em',
        marginRight: '2em',
        flip: false,
    },
    field: {
        marginRight: '1em',
        marginLeft: '1em',
    },
    buttons: {
        textAlign: "center",
        marginTop: "20px"
    }
}));

const PartnershipStep2 = ({ handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched }) => {
    const classes = useStyles();
    const [selectedDate, setSelectedDate] = React.useState(new Date());
    const handleDateChange = (date) => {
        setSelectedDate(date);
    };

    return (
        <div>
            <FormLabel component="legend" className={classes.title}>ועכשיו על השותפות..</FormLabel>
            <div className={classes.body}>
                <div className={classes.row}>
                    <div className={classes.field} style={{ marginTop: '-1em', width: '9em' }}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Field
                                required
                                as={KeyboardDatePicker} 
                                disableToolbar
                                variant="inline"
                                format="dd/MM/yyyy"
                                margin="normal"
                                name="date_of_sign"
                                label="תאריך חתימה"
                                value={values["date_of_sign"] ? values["date_of_sign"] : null}
                                onChange={(date) => handleChangeField(setFieldValue, "date_of_sign", date)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                    <Field
                        as={TextField}
                        required
                        name="partnership_name"
                        label="שם השותפות" 
                        className={classes.field} style={{ width: '15em' }}
                    />
                    <Field
                        as={TextField}
                        required 
                        name="partnership_idea"
                        label="עיקר השותפות" 
                        className={classes.field} style={{ width: '20em' }}
                    />
                </div>
                <div className={classes.row}>
                    <Field
                        as={TextField}
                        required 
                        name="partnership_goals"
                        label="מטרות השותפות"
                        className={classes.field} style={{ width: '48em' }}
                    />
                </div>
            </div>
            <div className={classes.buttons}>
                <Button
                    onClick={handleBack}
                >
                    הקודם
                    </Button>
                <Button onClick={() => handleNext(["date_of_sign", "partnership_name", "partnership_idea", "partnership_goals"], setFieldTouched, errors)} variant="contained" color="primary">
                    המשך
                  </Button>
            </div>
        </div>
    );
}

export default PartnershipStep2;