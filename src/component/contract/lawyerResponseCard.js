import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { LAWYER_SCREEN } from '../../constants/route';


const useStyles = makeStyles((theme) => ({
  title: {
    direction: "rtl",
    textAlign: "center"
  }
}));

const LawyerResponseCard = ({setOpen, open, onApprove, setDescription, title, history}) => {

  const classes = useStyles();

  const handleClose = () => {
    setOpen(false);
  };

  const handleApprove = () => {
    setOpen(false);
    onApprove();
    history.push(LAWYER_SCREEN);
  };

  return (
    <div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" fullWidth>
        <DialogTitle id="form-dialog-title" className={classes.title} >{title}</DialogTitle>
        <DialogContent>
          <TextField
            id="outlined-multiline-static"
            label="הערות"
            multiline
            rows={8}
            fullWidth
            placeholder="עורך דין יקר/ה, תאר/י בפירוט את הסיבה כאן"
            variant="outlined"
            onChange={(e) => setDescription(e.target.value)}
          />         
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            ביטול
          </Button>
          <Button onClick={handleApprove} color="primary">
            אישור
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default withRouter(LawyerResponseCard)