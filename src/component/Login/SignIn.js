import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import React from 'react';
import Loading from '../../component/feedback/loading';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
    width: '100vw'    
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  linkToSignIn: {
    marginRight: '35%',
    cursor: "pointer",
    flip: false
  },
  loading: {
    marginTop: '10%',
    marginRight: '47%',
    flip: false
  }
}));

const SignIn = (props) => {
  const classes = useStyles();
  const {onSignIn, onChangeField, setScreen, loading} = props;

  return (
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            התחברות
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="אימייל"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={(e) => onChangeField("email", e.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="סיסמא"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(e) => onChangeField("password", e.target.value)}
            />
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={() => onSignIn()}
            >
              התחבר\י
            </Button>
            <Grid container>          
              <Grid item className={classes.linkToSignIn}>
                <Link variant="body2" onClick={() => setScreen(false)}>
                  {"אין לך משתמש ? לחצ/י כאן"}
                </Link>
              </Grid>              
            </Grid>
            { loading ? (<div className={classes.loading}> <Loading/> </div>) : null }
          </form>
        </div>
      </Grid>
  );
}

export default SignIn;