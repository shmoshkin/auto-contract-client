import { IconButton, makeStyles, Tooltip } from "@material-ui/core";
import { Error } from "@material-ui/icons";
import React from 'react';

const useStyles = makeStyles(theme => ({
    iconButton: {
        padding: 2
    },
    tooltip: {
        backgroundColor: "rgba(255, 117, 177, 0.9)"
    }
}))

const ErrorTooltipIcon = ({ message }) => {
    const classes = useStyles();
    return(
        <Tooltip
            title={message}
            placement="bottom-start"
            classes={{
                tooltip: classes.tooltip
            }}
        >
            <IconButton>
                <Error color='error' fontSize='small'/>
            </IconButton>
        </Tooltip>
    )
}

export default ErrorTooltipIcon;