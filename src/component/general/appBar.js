import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { isNil, map } from 'ramda';
import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { HOME, LOGIN } from '../../constants/route';
import { clearToken, getAdmin, getName, getRole } from '../../utils/token';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  logout: {
    marginLeft: '1.5%'
  },
  accountIcon: {
    height: '110%'
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  inputRoot: {
    color: 'inherit',
  },
  sectionDesktop: {
    marginLeft: "30%",
    flip: false,
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  link: {
    cursor: 'pointer',
    color: "white",
    textDecoration: "blink"
  }
}));


const AppBarComponent = (props) => {

  const classes = useStyles();

  const { tabs, history } = props;

  const logOut = () => {
    clearToken();
    history.push(LOGIN);
  }

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
        <Link  to={HOME} className={classes.link}>
          <IconButton color="inherit">
              <Typography className={classes.title} variant="h6" noWrap>
                חוזליטו
            </Typography>
          </IconButton>
        </Link>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            {
              map((tab) => {
                const isVisible = (getAdmin() || isNil(tab.role) || tab.role === getRole());
                return (isVisible ? <div key={tab.title}>
                  <Link to={tab.link} className={classes.link}>
                    <IconButton color="inherit">
                      <Typography>{tab.title}</Typography>
                    </IconButton>
                  </Link>
                </div> : null)
              }, tabs)
            }
          </div>
          <div className={classes.logout}>
            <IconButton color="inherit" onClick={logOut}>
              <Typography>התנתקות</Typography>
            </IconButton>
          </div>
          <Tooltip title={getName()} placement="bottom">
            <IconButton
              className={classes.accountIcon}
              edge="end"
              aria-label="account of current user"
              aria-haspopup="true"
              color="inherit"
              tooltip="משתמש"
            >
              <AccountCircle />
            </IconButton>
          </Tooltip>

        </Toolbar>
      </AppBar>
    </div>
  );
}

export default withRouter(AppBarComponent);