import { createMuiTheme } from '@material-ui/core/styles';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import MaterialTable from 'material-table';
import React, { Component } from 'react';

class Table extends Component {

    constructor(props) {
      super(props);
      this.theme = createMuiTheme({
        palette: {
          primary: {
            main: '#1565c0',
          },
          secondary: {
            main: '#1565c0',
          },
        },
      });
    }

    render() {

      const {columns, data, title, onRowDelete, onRowUpdate, onRowAdd, grouping} = this.props;
      return (
        <MuiThemeProvider theme={this.theme}>
          <MaterialTable
            title={title}
            columns={columns}
            data={data}
 
            editable={{
            onRowAdd: onRowAdd ? ((newData) =>
              new Promise((resolve, reject) => {
                setTimeout(() => {
                  onRowAdd(newData);
                  resolve();
                }, 1000)
            })) : null,
            onRowUpdate: (newData, oldData) =>
                new Promise((resolve, reject) => {
                    setTimeout(() => {
                        onRowUpdate(newData, oldData)
                        resolve();
                    }, 1000);
                }),
            onRowDelete: onRowDelete ? ((oldData) =>
                new Promise((resolve, reject) => {
                    setTimeout(() => {
                        onRowDelete(oldData)
                        resolve();
                    }, 1000);
                })) : null
            }}
            options={{
              grouping: grouping ? grouping : false
            }}
            localization={{
              body: {
                emptyDataSourceMessage: 'אין רשומות להצגה',
                addTooltip: 'הוספה',
                deleteTooltip: 'מחיקה',
                editTooltip: 'עריכה',
                filterRow: {
                  filterTooltip: 'סינון'
                },
                editRow: {
                  deleteText: 'האם אתה בטוח שבצונך למחוק רשומה זו ?',
                  cancelTooltip: 'ביטול',
                  saveTooltip: 'אישור'
                }
              },
              grouping: {
                placeholder: 'גרור כותרות',
                groupedBy: 'מקובץ לפי'
              },header: {
                actions: 'פעולות'
              },
              pagination: {
                labelDisplayedRows: '{from}-{to} מתוך {count}',
                labelRowsSelect: 'שורות',
                labelRowsPerPage: 'שורות בעמוד:',
                firstAriaLabel: 'עמוד ראשון',
                firstTooltip: 'עמוד ראשון',
                previousAriaLabel: 'העמוד הקודם',
                previousTooltip: 'העמוד הקודם',
                nextAriaLabel: 'העמוד הבא',
                nextTooltip: 'העמוד הבא',
                lastAriaLabel: 'עמוד אחרון',
                lastTooltip: 'עמוד אחרון'
              },
              toolbar: {
                addRemoveColumns: 'הוסף או מחק עמודות',
                nRowsSelected: '{0} שורות (n) נבחרו',
                showColumnsTitle: 'הצג עמודות',
                showColumnsAriaLabel: 'הצג עמודות',
                exportTitle: 'יצא',
                exportAriaLabel: 'יצא',
                exportName: 'יצא כ csv',
                searchTooltip: 'חיפוש',
                searchPlaceholder: 'חיפוש'
              }
            }}
          />
        </MuiThemeProvider>
      )
    }
  }

export default Table;