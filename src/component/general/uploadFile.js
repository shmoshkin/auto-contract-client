import React from 'react';
import styles from '../../mystyle/upload.css'; 

const UploadFile = (props)  => {
   
    const {onChangeHandler} = props;
    return (
      <div>
        <div className="col-md-6">
            <form method="post" action="#" id="#">
                <div className="form-group files color">
                  <label>Upload Your File </label>
                  <input type="file" className="form-control" multiple="" onChange={(e) =>onChangeHandler(e)}></input>
                </div>
            </form>
        </div>
      </div>
    );
  }
  
  export default UploadFile;