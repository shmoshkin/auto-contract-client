import { Chart, CommonSeriesSettings, SeriesTemplate, Title } from 'devextreme-react/chart';
import React from 'react';

class BarChart extends React.Component {
  render() {
      const {title, data} = this.props;

    return (
      <Chart
        id="chart"
        palette="Soft"
        dataSource={data}>
        <CommonSeriesSettings
          argumentField="age"
          valueField="number"
          type="bar"
          ignoreEmptyPoints={true}
        />
        <SeriesTemplate nameField="age" />
        <Title
          text={title}
          subtitle=""
        />
      </Chart>
    );
  }
}

export default BarChart;