import { useMutation, useSubscription } from '@apollo/react-hooks';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/styles';
import React from 'react';
import Table from '../../component/general/table';
import { DELETE_USER_BY_EMAIL, SUBSCRIPTION_GET_ALL_USERS, UPDATE_USER_BY_EMAIL } from '../../graphql/queries/users';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: 1,
      width: "100%",
      height: "100%"
    }    
  },
  loading: {
    position: 'absolute',
    left: '50%',
    top: '50%'
  }
});

const ManageUsersContainer = ({classes}) => {

  const onRowDelete = (oldData) => {
    deleteUser({ variables: { email: oldData.email } })
  }
  
  const onRowUpdate = (newData, oldData) => {
    updateUser({ variables: { email: oldData.email, role: newData.role } })
  }

  const [updateUser, updateRes] = useMutation(UPDATE_USER_BY_EMAIL);
  const [deleteUser, deleteRes] = useMutation(DELETE_USER_BY_EMAIL);

  const {
    data,
    loading,
    error
  } = useSubscription(SUBSCRIPTION_GET_ALL_USERS);

  if (loading) return (null)
  if (error) return <p>error</p>

  return (
    <div className={classes.root}>
      <Paper elevation={3}>
        <Table
          columns={[
            {
              title: 'שם', field: 'name',
              render: rowData => `${rowData.firstName} ${rowData.lastName}`,
              editable: 'never'
            },
            { title: 'אימייל', field: 'email', editable: 'never' },
            { title: 'תפקיד', field: 'role'},
            { title: 'תאריך יצירה', field: 'created_at', type:'date', editable: 'never'},
          ]}
          data={data.USERS}
          title="משתמשים"
          onRowUpdate={onRowUpdate}
          onRowDelete={onRowDelete}
        />
      </Paper>
    </div>
  )
}

export default withStyles(styles)(ManageUsersContainer)