import React, { Component } from 'react';
import AppBarComponent from '../../component/general/appBar';
import { CHOOSE_CONTRACT, LAWYER_SCREEN, MANAGER, MY_CONTRACTS } from '../../constants/route';


class AppBarContainer extends Component {

    state = {
        tabs:[
            {title: "החוזים שלי", link: MY_CONTRACTS},
            {title: "יצירת חוזה", link: CHOOSE_CONTRACT},
            {title: "מסך עורך דין", link: LAWYER_SCREEN, role: "lawyer"},
            {title: "מנהל", link: MANAGER, role: "admin"},
        ],
        userCardOpen: false
    }

    onClickUserCard = () => {
        this.setState({userCardOpen: !this.state.userCardOpen})
    }

    render() {
        return (
            <AppBarComponent
                tabs={this.state.tabs}
            />
        )
    }
}

export default AppBarContainer