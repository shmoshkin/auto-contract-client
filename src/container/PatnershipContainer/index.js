import { useMutation } from '@apollo/react-hooks';
import Button from '@material-ui/core/Button';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Form, Formik } from 'formik';
import { forEach, isEmpty, isNil, map } from 'ramda';
import React from 'react';
import { withRouter } from 'react-router-dom';
import * as Yup from 'yup';
import AdditonalClauses from '../../component/contract/additionalClauses';
import Step1 from '../../component/contract/partnership/step1';
import Step2 from '../../component/contract/partnership/step2';
import Step3 from '../../component/contract/partnership/step3';
import Payment from '../../component/contract/payment';
import SuggestedClauses from '../../component/contract/suggestedClauses';
import DynamicSnackbar from '../../component/feedback/snackbar';
import { LAWYER_SCREEN, MY_CONTRACTS } from '../../constants/route';
import { DETELE_CLAUSES_FOR_SUGGEST_BY_ID, DETELE_CLAUSES_FOR_SUGGEST_IN_CONTRACT_BY_ID } from '../../graphql/queries/clauses';
import { ADD_CLAUSES_FOR_SUGGEST, ADD_CLAUSES_IN_CONTRACT, ADD_PARTNERSHIP_CONTRACT, UPDATE_CLAUSES_FOR_SUGGEST, UPDATE_PARTNERSHIP_CONTRACT } from '../../graphql/queries/contracts';
import { getEmail, getRole } from '../../utils/token';
import General from '../StartPageContractContainer';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  buttons: {
    position: 'absolute',
    bottom: '2em',
    left: '47%',
  },
  button: {
    marginLeft: '1em',
    marginRight: '1em',
  },
}));

function getSteps() {
  return ['מידע כללי', 'כמה פרטים על השותפים וממשיכים', 'ועכשיו על השותפות', 'עוד כמה פרטים נוספים', 'הנה כמה הצעות לסעיפים מאיתנו...', 'בא לך להוסיף סעיפים נוספים?', 'תשלום'];
}

function getStepContent(stepIndex, handleNext, handleBack, values, setFieldValue, errors, touched, setFieldTouched, dirty, isValid, validateField, validateForm) {
  switch (stepIndex) {
    case 0:
      return <General handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid} validateField={validateField} validateForm={validateForm}></General>;
    case 1:
      return <Step1 handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></Step1>;
    case 2:
      return <Step2 handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></Step2>;
    case 3:
      return <Step3 handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></Step3>;
    case 4:
      return <SuggestedClauses handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}
        type='PARTNERSHIP_CONTRACT'
      />;
    case 5:
      return <AdditonalClauses handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></AdditonalClauses>;
    case 6:
      return <Payment handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></Payment>;
    default:
      return 'Unknown stepIndex';
  }
}

const PartnershipContractContainer = ({ history, location: { isUpdate: isUpdate = false, contractId: contractId, contract: contract, isFromLawyer } }) => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(isFromLawyer ? 5 : 0);
  const [snackbar, setSnackbar] = React.useState({ open: false, message: "" });

  const steps = getSteps();

  const handleUpdateMode = () => {
    let contractCopy = contract;
    let additionalClauses = [];
    let suggestedClauses = {};

    contractCopy.clauses_in_contract.forEach(clause_in_contract => {
      if (clause_in_contract.clauseid.tag === "no tag") {
        additionalClauses.push(clause_in_contract.clauseid);
      }
      else {
        suggestedClauses[clause_in_contract.clauseid.tag] = clause_in_contract.clauseid.id;
      }
    })

    contractCopy.additionalClauses = additionalClauses;
    contractCopy.suggestedClauses = suggestedClauses;
    return contractCopy;
  }

  const [initialValues, setInitialValues] = React.useState(contract ? handleUpdateMode() : {});

  const schema = Yup.object().shape({
    title: Yup.string().required('יש למלא את שם החוזה'),
    "a_person_name": Yup.string().required('יש למלא את שם צד א'),
    "a_person_id": Yup.number().required('יש למלא את תז צד א'),
    "a_person_address": Yup.string().required('יש למלא את כתובת צד א'),
    "b_person_name": Yup.string().required('יש למלא את שם צד ב'),
    "b_person_id": Yup.number().required('יש למלא את תז צד ב'),
    "b_person_address": Yup.string().required('יש למלא את כתובת צד ב'),
    "date_of_sign": Yup.date().required("יש למלא תאריך העסקה"),
    "partnership_name": Yup.string().required('יש למלא את שם השותפות'), 
    "partnership_idea": Yup.string().required('יש למלא את עיקר השותפות'), 
    "partnership_goals": Yup.string().required('יש למלא את מטרות השותפות'),

    "a_person_percents": Yup.number().required('יש למלא את חלק צד א (באחוזים)'), 
    "a_investment": Yup.number().required('יש למלא את ההון ההתחלתי שישקיע צד א'), 
    "a_person_job": Yup.string().required('יש למלא את עיסוקו של צד א'), 
    "b_person_percents": Yup.string().required('יש למלא את חלק צד א (באחוזים)'), 
    "b_investment": Yup.number().required('יש למלא את ההון ההתחלתי שישקיע צד ב'), 
    "b_person_job": Yup.string().required('יש למלא את עיסוקו של צד ב'), 
    "end_investment_date": Yup.date().required('יש למלא את תאריך תום ההשקעה'), 
    "details_on_calc_books": Yup.string().required('יש למלא את העניינים שירשמו בספרי חשבונות'),
    "years_of_no_competition":  Yup.number().required('יש למלא את הזמן ללא תחרות'),
    "court_for_contract": Yup.string().required('יש למלא את בית המשפט הרשאי לעסוק')
  })

  const handleNext = (fields, setFieldTouched, errors) => {

    const found = Object.keys(errors).some(errField=> fields.includes(errField))

    fields.forEach(field => {
      setFieldTouched(field, true);
    });

    if(!found) {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const [addPartnershipContract, addPartnershipContractRes] = useMutation(ADD_PARTNERSHIP_CONTRACT);
  const [updatePartnershipContract, updatePartnershipContractRes] = useMutation(UPDATE_PARTNERSHIP_CONTRACT);
  const [addClausesForSuggest, addClausesForSuggestRes] = useMutation(ADD_CLAUSES_FOR_SUGGEST);
  const [updateClausesForSuggest, updateClausesForSuggestRes] = useMutation(UPDATE_CLAUSES_FOR_SUGGEST);
  const [addClausesInContract, addClausesInContractRes] = useMutation(ADD_CLAUSES_IN_CONTRACT);
  const [deleteClausesInContract, deleteClausesInContractRes] = useMutation(DETELE_CLAUSES_FOR_SUGGEST_IN_CONTRACT_BY_ID);
  const [deleteClausesForSuggest, deleteClausesForSuggestRes] = useMutation(DETELE_CLAUSES_FOR_SUGGEST_BY_ID);
  
  const handleSubmit = async (values) => {
    const { a_investment,
      a_person_address,
      a_person_id,
      a_person_job,
      a_person_name,
      a_person_percents,
      b_investment,
      b_person_address,
      b_person_id,
      b_person_job,
      b_person_name,
      b_person_percents,
      court_for_contract,
      date_of_sign,
      description,
      details_on_calc_books,
      end_investment_date,
      partnership_idea,
      partnership_goals,
      partnership_name,
      title,
      years_of_no_competition,
      suggestedClauses,
      additionalClauses,
      clauses_in_contract } = values;

    const isWaitingForLawyer = (isNil(additionalClauses) || isEmpty(additionalClauses)) ? false : true;
    const status = isWaitingForLawyer ? "מחכה לאישור" : "מאושר";
    const price = isWaitingForLawyer ? 500 : 250; 

    if(!isUpdate) {
      const resContract = await (addPartnershipContract({
        variables: {
          a_investment: a_investment,
          a_person_address: a_person_address,
          a_person_id: a_person_id,
          a_person_job: a_person_job,
          a_person_name: a_person_name,
          a_person_percents: a_person_percents,
          b_investment: b_investment,
          b_person_address: b_person_address,
          b_person_id: b_person_id,
          b_person_job: b_person_job,
          b_person_name: b_person_name,
          b_person_percents: b_person_percents,
          court_for_contract: court_for_contract,
          date_of_sign : date_of_sign.toISOString(),
          description: description,
          details_on_calc_books: details_on_calc_books,
          end_investment_date: end_investment_date.toISOString(),
          partnership_idea: partnership_idea,
          partnership_goals: partnership_goals,
          partnership_name: partnership_name,
          title: title,        
          years_of_no_competition: years_of_no_competition,        
          user_id: getEmail(),
          status: status,
          price: price
        }
      }))
  
      const clausesObjects = map((obj) => {
        return { "contract_type": "PARTNERSHIP_CONTRACT", "text": obj.text }
      }, additionalClauses ? additionalClauses : [])
  
      const resClausesForSuggest = await (addClausesForSuggest({ variables: { objects: clausesObjects } }))
  
      var clausesInContractObjects = map((obj) => {
        return {
          "contract_id": resContract.data.insert_PARTNERSHIP_CONTRACTS.returning[0].contract_id
          , "clause_id": obj.id
        }
      }, resClausesForSuggest.data.insert_CLAUSES_FOR_SUGGEST.returning
      )
  
      for (let suggest in suggestedClauses) {
        clausesInContractObjects.push({
          "contract_id": resContract.data.insert_PARTNERSHIP_CONTRACTS.returning[0].contract_id
          , "clause_id": suggestedClauses[suggest]
        })
      }
  
      const resClausesInContract = await (addClausesInContract({ variables: { objects: clausesInContractObjects } }))
  
      if (resContract && resClausesForSuggest && resClausesInContract) {
        setSnackbar({ open: true, message: "החוזה נוצר בהצלחה" })
      } else {
        setSnackbar({ open: true, message: "משהו השתבש, נסו מאוחר יותר" })
      }
    }
    else {
      const resContract = await (updatePartnershipContract({
        variables: {
          a_investment: a_investment,
          a_person_address: a_person_address,
          a_person_id: a_person_id,
          a_person_job: a_person_job,
          a_person_name: a_person_name,
          a_person_percents: a_person_percents,
          b_investment: b_investment,
          b_person_address: b_person_address,
          b_person_id: b_person_id,
          b_person_job: b_person_job,
          b_person_name: b_person_name,
          b_person_percents: b_person_percents,
          court_for_contract: court_for_contract,
          date_of_sign : date_of_sign,
          description: description,
          details_on_calc_books: details_on_calc_books,
          end_investment_date: end_investment_date,
          partnership_idea: partnership_idea,
          partnership_goals: partnership_goals,
          partnership_name: partnership_name,
          title: title,        
          years_of_no_competition: years_of_no_competition,        
          user_id: getEmail(),
          status: status,
          price: price,
          contractId: contract.contract_id,
        }
      }))

      // 1. delete the clasues in contract of the updated contract
      const resClausesInContractDeleted = await (deleteClausesInContract({ variables: { contract_id: contract.contract_id } }))

      let withId = [];
      additionalClauses.forEach(clause => {
        if(clause.id != null) {
          withId.push({ "contract_type": "PARTNERSHIP_CONTRACT", "text": clause.text, "id": clause.id })
        }
      });

      // 2. delete the clauses for suggest of the updated contract
      forEach(async (obj) => {
        await deleteClausesForSuggest({ variables: { clause_id: obj.id } });
      }, withId)

      let withoutId = [];
      additionalClauses.forEach(clause => {
        if(clause.id == null) {
          withoutId.push({ "contract_type": "PARTNERSHIP_CONTRACT", "text": clause.text })
        }
      });

      // 3. add clauses for suggest (the ones with id are the clauses that were before)
      const resClausesForSuggestWithId = await (addClausesForSuggest({ variables: { objects: withId } }))
      const resClausesForSuggestWithoutId = await (addClausesForSuggest({ variables: { objects: withoutId } }))

      var clausesInContractObjects2 = map((obj) => {
        return {
          "contract_id": contract.contract_id
          , "clause_id": obj.id
        }
      }, resClausesForSuggestWithId.data.insert_CLAUSES_FOR_SUGGEST.returning
      )

      for (let clause in resClausesForSuggestWithoutId.data.insert_CLAUSES_FOR_SUGGEST.returning) {
        clausesInContractObjects2.push({
          "contract_id": contract.contract_id
          , "clause_id": resClausesForSuggestWithoutId.data.insert_CLAUSES_FOR_SUGGEST.returning[clause].id
        })
      }

      for (let suggest in suggestedClauses) {
        clausesInContractObjects2.push({
          "contract_id": contract.contract_id
          , "clause_id": suggestedClauses[suggest]
        })
      }

      // 4. update the clauses in contract (the additional + sugggested)
      const resClausesInContract2 = await (addClausesInContract({ variables: { objects: clausesInContractObjects2 } }))
      
      // let additional = clauses_in_contract.map(clause => { return { id: clause.clauseid.id, text: clause.clauseid.text } })

      // forEach(async (obj) => {
      //   await updateClausesForSuggest({ variables: { clauseId: obj.id, text: obj.text } });
      // }, additional)

      if(resContract){
        setSnackbar({open: true, message: "החוזה עודכן בהצלחה"})
      } else {
        setSnackbar({open: true, message: "משהו השתבש, נסו מאוחר יותר"})
      }
    }

    if(getRole() === "lawyer") {
      setTimeout(function(){ history.push(LAWYER_SCREEN); }, 1000);
    }
    else {
      setTimeout(function(){ history.push(MY_CONTRACTS); }, 1000);
    }
  }

  return (
    <>
    <Formik
      initialValues={initialValues}
      validationSchema={schema}
      onSubmit={handleSubmit}
    >
      {({ values, setFieldValue, errors, touched, setFieldTouched, dirty, isValid, validateField, validateForm }) => {
        return (
          <Form>
            <div className={classes.root}>
              <Stepper activeStep={activeStep} alternativeLabel>
                {steps.map((label) => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
              <div>
                {activeStep === steps.length ? (
                  <div>
                    <Typography className={classes.instructions}>All steps completed</Typography>
                    <Button onClick={handleReset}>Reset</Button>
                  </div>
                ) : (
                    <div className={classes.backButton}>
                      <div className={classes.instructions}>{getStepContent(activeStep, handleNext, handleBack, values, setFieldValue, errors, touched, setFieldTouched, dirty, isValid, validateField, validateForm)}</div>
                    </div>
                  )}
              </div>
            </div>
          </Form>
        )
      }}
    </Formik>
     <DynamicSnackbar
     open={snackbar.open}
     setOpen={() => setSnackbar({ open: true, message: snackbar.message })}
     message={snackbar.message}
    />
   </>
  );
}

export default withRouter(PartnershipContractContainer)