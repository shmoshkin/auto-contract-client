import { withStyles } from '@material-ui/styles';
import React from 'react';
import ManagerStatisticsContainer from '../ManagerStatisticsContainer';
import ManageUsersContainer from '../ManageUsersContainer';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: 1,
      width: "100%",
      height: "100%"
    }    
  },
  loading: {
    position: 'absolute',
    left: '50%',
    top: '50%'
  }
});

const ManagerContainer = ({classes}) => {  
  return (
    <div className={classes.root}>
        <ManagerStatisticsContainer/>
        <ManageUsersContainer/>
    </div>
  )
}

export default withStyles(styles)(ManagerContainer)