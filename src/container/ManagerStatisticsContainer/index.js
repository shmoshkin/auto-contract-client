import { useQuery } from '@apollo/react-hooks';
import { green, red, yellow } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/styles';
import { concat, filter, path, pathEq } from 'ramda';
import React from 'react';
import Loading from '../../component/feedback/loading';
import BarChart from '../../component/general/barGraph';
import PieGraph from '../../component/general/pieGraph';
import { QUERY_GET_ALL_CONTRACTS } from '../../graphql/queries/contracts';
import { PRICES_SUM_OF_CONTRACTS } from '../../graphql/queries/manager';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: 1,
      width: "100%",
      height: "100%"
    }    
  },
  paper: {
    width: '20%',
    height: '180px'
  },
  bar:{
    marginTop: '3%',
    width: '50%',
  },
  graph: {
    marginRight: '65%',
    marginTop: '-25%',
    flip:false
  },
  text: {
    marginTop: '-12%',
    marginRight: '26%',
    textAlign:'right'
  },
  title:{
    textAlign: 'center'
  },
  loading: {
    position: 'absolute',
    left: '50%',
    top: '50%'
  },
  red: {
    marginTop: '7%',
    // marginBottom: '5%',
    marginRight: '5%',
    backgroundColor: red[500],
  },
  yellow: {
    marginRight: '5%',
    marginTop: '7%',
    backgroundColor: yellow[500],
  },
  green: {
    marginRight: '5%',
    marginTop: '7%',
    backgroundColor: green[500],
  },
});

const ManageStatisticContainer = ({classes}) => {

  const allContractsRes = useQuery(QUERY_GET_ALL_CONTRACTS);
  const contractsPriceSumRes = useQuery(PRICES_SUM_OF_CONTRACTS);

  if (allContractsRes.loading || contractsPriceSumRes.loading) return (<div className={classes.loading}><Loading/></div>)
  if (allContractsRes.error || contractsPriceSumRes.error) return <p>error</p>
  
  const allContracts = concat(concat(concat(
    allContractsRes.data.APP_RENTAL_CONTRACTS,
    allContractsRes.data.CAR_CONTRACTS),
    allContractsRes.data.SECRET_CONTRACTS),
    allContractsRes.data.PARTNERSHIP_CONTRACTS
  )
  
  const approved = filter(pathEq(['status'], "מאושר"), allContracts)
  const waitingToApprove = filter(pathEq(['status'], "מחכה לאישור"), allContracts)
  const notApproved = filter(pathEq(['status'], "לא מאושר"), allContracts)

  const statusChartData = [
    {key: "מאושר", value: approved.length},
    {key: "מחכה לאישור", value: waitingToApprove.length},
    {key: "לא מאושר", value: notApproved.length}
  ]

  const typeChartData = [
    {key: "השכרת דירה", value: allContractsRes.data.APP_RENTAL_CONTRACTS.length},
    {key: "מכירת / השכרת רכב", value: allContractsRes.data.CAR_CONTRACTS.length},
    {key: "הסכם שותפות", value: allContractsRes.data.PARTNERSHIP_CONTRACTS.length},
    {key: "הסכם סודיות", value: allContractsRes.data.SECRET_CONTRACTS.length}
  ]
  
  const pricesSum = [
    { age: 'שכירות דירה', number: path(['data', 'APP_RENTAL_CONTRACTS_aggregate', 'aggregate', 'sum', 'price'], contractsPriceSumRes)},
    { age: 'מכירת / קניית רכב', number: path(['data', 'CAR_CONTRACTS_aggregate', 'aggregate', 'sum', 'price'], contractsPriceSumRes) },
    { age: 'הסכם שותפות', number: path(['data', 'PARTNERSHIP_CONTRACTS_aggregate', 'aggregate', 'sum', 'price'], contractsPriceSumRes)},
    { age: 'הסכם סודיות', number: path(['data', 'SECRET_CONTRACTS_aggregate', 'aggregate', 'sum', 'price'], contractsPriceSumRes)}
  ]

  return (
    <div className={classes.root}>
      <div className={classes.bar}> 
        <BarChart 
          title="הכנוסת לפי חוזה"
          data={pricesSum}
          />
      </div>
      <div className={classes.graph}> 
        <PieGraph
          title={"חוזים לפי סטטוס"}
          data={statusChartData}
        />
        <PieGraph
          title={"חוזים לפי סוג"}
          data={typeChartData}
        />
      </div>
    </div>
  )
}

export default withStyles(styles)(ManageStatisticContainer)