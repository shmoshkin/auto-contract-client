import { useMutation, useQuery, useSubscription } from '@apollo/react-hooks';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/styles';
import { concat, map, prop, sortBy } from 'ramda';
import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import ContractView from '../../component/contract/contractView';
import LawyerResponseCard from '../../component/contract/lawyerResponseCard';
import Loading from '../../component/feedback/loading';
import DynamicSnackbar from '../../component/feedback/snackbar';
import Table from '../../component/general/table';
import { ADD_CLAUSES_FOR_SUGGEST, DELETE_CLAUSES_FOR_SUGGEST_BY_ID, SUBSCRIPTION_ALL_CLAUSES_FOR_SUGGEST, UPDATE_CLAUSES_FOR_SUGGEST_BY_ID } from '../../graphql/queries/clauses';
import { QUERY_GET_ALL_WAITING_TO_APPROVE_CONTRACTS, UPDATE_APP_CONTRACT_STATUS, UPDATE_CAR_CONTRACT_STATUS, UPDATE_PARTNERSHIP_CONTRACT_STATUS, UPDATE_SECRET_CONTRACT_STATUS } from '../../graphql/queries/contracts';

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    marginTop: "5%",
    marginRight: "2.5%"
  },
  noContract: {
    textAlign: 'center'
  },
  loading: {
    marginTop: '20%',
    marginRight: '50%',
    flip: false
  },
  button: {
    marginTop: '3%',
    width: '15%'
  },
  paper: {
    textAlign: 'center',
  },
  contracts:{
    marginBottom: '2%',
    marginRight: '2%',
    flip:false
  },
  clauses: {
    marginRight: '2%',
    marginTop: '2%',
    flip:false
  },
  divider: {
    width: '100%',
    marginRight: '1%',
    flip:false
  },
});

const LawyerContainer = ({classes, history}) => {

    const [lawyerResponseCard, setLawyerResponseCard] = useState({open: false, description: ""});
    const [title, setTitle] = useState("");
    const [selectedContract, setSelectedContract] = useState(null);
    const [status, setStatus] = useState(null);
    const [snackbar, setSnackbar] = useState({open: false, message: ""});
    
    const setOpen = (open) => setLawyerResponseCard({open:open})
    const setDescription = (desc) => setLawyerResponseCard({description:desc, open: lawyerResponseCard.open});
    
    const setStatusAction = async () => {

        const statusToChange = status ? "מאושר" : "לא מאושר";

        let res;
        switch(selectedContract.type) {
            case "CAR_RENTAL":                
                res = await updateCarContract({ variables: { contract_id: selectedContract.contract_id, lawyer_description: lawyerResponseCard.description ? lawyerResponseCard.description : "", status: statusToChange } })
              break;
            case "SECRET_CONTRACT":
                res = await updateSecretContract({ variables: { contract_id: selectedContract.contract_id, lawyer_description: lawyerResponseCard.description ? lawyerResponseCard.description : "", status: statusToChange } })
              break;
            case "PARTNERSHIP_CONTRACT":
                res = await updatePartnershipContract({ variables: { contract_id: selectedContract.contract_id, lawyer_description: lawyerResponseCard.description ? lawyerResponseCard.description : "", status: statusToChange } })
              break;
            case "APP_RENTAL":
                res = await updateAppContract({ variables: { contract_id: selectedContract.contract_id, lawyer_description: lawyerResponseCard.description ? lawyerResponseCard.description : "", status: statusToChange } })
              break;
            default:
          }

          res ? setSnackbar({open: true, message: "שינוי סטטוס החוזה בוצע בהצלחה"}) : setSnackbar({open: true, message: "קרתה שגיאה בעדכון הסטטוס, נסה מאוחר יותר"}) 
    }

    const onRowDelete = (oldData) => {
      deleteClause({ variables: { clause_id: oldData.id } })
      setSnackbar({open:true, message: "רשומה נמחקה בהצלחה"})
    }
    
    const onRowUpdate = (newData, oldData) => {
      updateClause({ variables: { clause_id: oldData.id, tag: newData.tag, text: newData.text } })
      setSnackbar({open:true, message: "רשומה התעדכנה בהצלחה"})
    }

    const onRowAdd = (newData) => {
      addClause({ variables: { contract_type: newData.contract_type, tag: newData.tag, text: newData.tag } })
      setSnackbar({open:true, message: "רשומה נוספה בהצלחה"})
    }
    
    const [updateCarContract, updateCarRes] = useMutation(UPDATE_CAR_CONTRACT_STATUS);
    const [updateAppContract, updateAppRes] = useMutation(UPDATE_APP_CONTRACT_STATUS);
    const [updatePartnershipContract, updatePartnershipRes] = useMutation(UPDATE_PARTNERSHIP_CONTRACT_STATUS);
    const [updateSecretContract, updateSecretRes] = useMutation(UPDATE_SECRET_CONTRACT_STATUS);
    const [updateClause, updateClauseRes] = useMutation(UPDATE_CLAUSES_FOR_SUGGEST_BY_ID)
    const [deleteClause, deleteClauseRes] = useMutation(DELETE_CLAUSES_FOR_SUGGEST_BY_ID)
    const [addClause, addClauseRes] = useMutation(ADD_CLAUSES_FOR_SUGGEST)

    const waitingToApproveContracts = useQuery(QUERY_GET_ALL_WAITING_TO_APPROVE_CONTRACTS);

    const clausesForSuggest = useSubscription(SUBSCRIPTION_ALL_CLAUSES_FOR_SUGGEST);
    if (waitingToApproveContracts.loading || clausesForSuggest.loading) return (<div className={classes.loading}><Loading/></div>)
    if (waitingToApproveContracts.error || clausesForSuggest.error) return <p>error</p>
    
    const allContracts = concat(concat(concat(
      waitingToApproveContracts.data.APP_RENTAL_CONTRACTS,
      waitingToApproveContracts.data.CAR_CONTRACTS),
      waitingToApproveContracts.data.SECRET_CONTRACTS),
      waitingToApproveContracts.data.PARTNERSHIP_CONTRACTS
    )

    const allContractsSortByUpdateDate = sortBy(prop("updated_at"))(allContracts);    

    return (
        <div className={classes.root} dir="rtl">
          <div className={classes.contracts}>
            {allContracts.length > 0 ?
              <div className={classes.root}>              
              <Grid container spacing={3}>
                  {map(contract => {
                      return (
                          <Grid key={contract.contract_id} item xs={4}>
                              <ContractView
                                  history={history}
                                  contract={contract} 
                                  isTranslate={false} 
                                  isEdit={true}
                                  isCancel={true}
                                  isApprove={true}
                                  onCancel={(contract) => {
                                      setOpen(true);
                                      setTitle("הערות לשיפור החוזה")
                                      setSelectedContract(contract)
                                      setStatus(false);
                                  }}
                                  onApprove={(contract) => {
                                      setOpen(true);
                                      setTitle("הערות לאישור החוזה")
                                      setSelectedContract(contract)
                                      setStatus(true);
                                  }}
                                  isFromLawyer={true}
                              />
                          </Grid>
                      )
                  },
                  allContractsSortByUpdateDate)}
              </Grid>
              <LawyerResponseCard
                  setOpen={setOpen}
                  open={lawyerResponseCard.open}
                  onApprove={setStatusAction}
                  onCancel={setStatusAction}
                  setDescription={setDescription}
                  title={title}
              />
              </div> :
              <div className={classes.noContract}>
                <Typography variant="h3" component="h2">
                    אין חוזים בסטטוס ממתין לאישור
                </Typography>
              </div>
              }
            </div>
            <Divider className={classes.divider}/>
            <div className={classes.clauses}>
              <Table
                columns={[
                  {
                    title: 'תיוג', field: 'tag',                                  
                  },
                  { title: 'סוג חוזה', field: 'contract_type'},
                  // render: rowData => {
                  //   let res;

                  //   switch(rowData.contract_type) {
                  //     case "CAR_RENTAL":                
                  //         res = "חוזה רכב"
                  //       break;
                  //     case "APP_RENTAL":
                  //         res = "חוזה שכירות דירה"
                  //       break;
                  //     case "PARTNERSHIP_CONTRACT":
                  //         res = "חוזה שותפות"
                  //       break;
                  //     case "SECRET_CONTRACT":
                  //         res = "חוזה סודיות"
                  //       break;
                  //     default:
                  //   }
                    
                  //   return res;
                  // }},
                  { title: 'סעיף', field: 'text'},
                ]}
                data={clausesForSuggest.data.CLAUSES_FOR_SUGGEST}
                title="סעיפים"
                onRowUpdate={onRowUpdate}
                onRowDelete={onRowDelete}
                onRowAdd={onRowAdd}
                grouping={true}
              />
            </div>
              <DynamicSnackbar
                open={snackbar.open}
                setOpen={() => setSnackbar({open:true, message: snackbar.message})}
                message={snackbar.message}
              />
        </div>
    )
    }

export default withStyles(styles)(withRouter(LawyerContainer))