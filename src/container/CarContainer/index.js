import { useMutation } from '@apollo/react-hooks';
import Button from '@material-ui/core/Button';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import to from 'await-to-js';
import { Form, Formik } from "formik";
import { assoc, countBy, forEach, isEmpty, isNil, map, pluck, prop, sortBy } from 'ramda';
import React from 'react';
import { withRouter } from 'react-router-dom';
import * as Yup from 'yup';
import AdditonalClauses from '../../component/contract/additionalClauses';
import Step1 from '../../component/contract/car/step1';
import Step2 from '../../component/contract/car/step2';
import Step3 from '../../component/contract/car/step3';
import Payment from '../../component/contract/payment';
import SuggestedClauses from '../../component/contract/suggestedClauses';
import DynamicSnackbar from '../../component/feedback/snackbar';
import { LAWYER_SCREEN, MY_CONTRACTS } from '../../constants/route';
import { DETELE_CLAUSES_FOR_SUGGEST_BY_ID, DETELE_CLAUSES_FOR_SUGGEST_IN_CONTRACT_BY_ID } from '../../graphql/queries/clauses';
import { ADD_CAR_CONTRACT, ADD_CLAUSES_FOR_SUGGEST, ADD_CLAUSES_IN_CONTRACT, GET_CLAUSES_BY_CONTRACT_ID, QUERY_GET_SIMILAR_CAR_CONTRACTS, UPDATE_CAR_CONTRACT, UPDATE_CLAUSES_FOR_SUGGEST } from '../../graphql/queries/contracts';
import { hasuraInstance } from '../../rest/axios';
import { getEmail, getRole } from '../../utils/token';
import General from '../StartPageContractContainer';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  buttons: {
    position: 'absolute',
    bottom: '2em',
    left: '47%',
  },
  button: {
    marginLeft: '1em',
    marginRight: '1em',
  },
}));

function getSteps() {
  return ['מידע כללי', 'כמה פרטים על העסקה וממשיכים', 'כמה פרטים על הרכב וממשיכים', 'נדבר רגע על התשלום', 'הנה כמה הצעות לסעיפים מאיתנו...', 'בא לך להוסיף סעיפים נוספים?', 'תשלום'];
}

function getStepContent(stepIndex, handleNext, handleBack, values, setFieldValue, errors, touched, setFieldTouched, dirty, isValid, validateField, validateForm, getSimilarData) {
  switch (stepIndex) {
    case 0:
      return <General handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid} validateField={validateField} validateForm={validateForm}></General>;
    case 1:
      return <Step1 handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></Step1>;
    case 2:
      return <Step2 handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></Step2>;
    case 3:
      return <Step3 handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></Step3>;
    case 4:
      return <SuggestedClauses
        handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid} getSimilarData={getSimilarData}
        type='CAR_RENTAL'
      />;
    case 5:
      return <AdditonalClauses handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></AdditonalClauses>;
    case 6:
      return <Payment handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></Payment>;
    default:
      return 'Unknown stepIndex';
  }
}

const initial = {
  buyer_id: 2094847,
  buyer_name: "עמית שמושקין",
  car_brand: "מאזדה",
  car_id: 4578874,
  car_is_hit: "no",
  car_km: 50000,
  car_price: 40000,
  car_year: 2011,
  owner_id: 2098337,
  owner_name: "נווה נגבי",
  place_of_sign: "הוד השרון",
  title: "חוזה רכב 8.2020",
  // suggestedClauses: {
  //   tag11: 55,
  //   tag22: 69
  // },
  date_of_sign: "2-8-2020",
  delivery_date: new Date(),
  payment_date: new Date(),
  car_test_end: new Date()

}

const CarContainer = ({ history, location: { isUpdate: isUpdate = false, contractId: contractId, contract: contract, isFromLawyer } }) => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(isFromLawyer ? 5 : 0);
  const [snackbar, setSnackbar] = React.useState({ open: false, message: "" });

  const steps = getSteps();

  const handleUpdateMode = () => {
    let contractCopy = contract;
    let additionalClauses = [];
    let suggestedClauses = {};

    contractCopy.clauses_in_contract.forEach(clause_in_contract => {
      if (clause_in_contract.clauseid.tag === "no tag") {
        additionalClauses.push(clause_in_contract.clauseid);
      }
      else {
        suggestedClauses[clause_in_contract.clauseid.tag] = clause_in_contract.clauseid.id;
      }
    })

    contractCopy.additionalClauses = additionalClauses;
    contractCopy.suggestedClauses = suggestedClauses;
    return contractCopy;
  }

  const [initialValues, setInitialValues] = React.useState(contract ? handleUpdateMode() : {});

  const schema = Yup.object().shape({
    title: Yup.string().required('יש למלא את שם החוזה'),
    "place_of_sign": Yup.string().required('יש למלא את מקום העסקה'),
    "date_of_sign": Yup.date().required("יש למלא תאריך העסקה"),
    "owner_name": Yup.string().required('יש למלא את שם המוכר'),
    "owner_id": Yup.number().required('יש למלא את תז המוכר'),
    "buyer_name": Yup.string().required('יש למלא את שם הקונה'),
    "buyer_id": Yup.number().required('יש למלא את תז הקונה'),
    "car_id": Yup.number().required('יש למלא את מספר הרכב'),
    "car_brand": Yup.string().required('יש למלא את סוג הרכב'),
    "car_year": Yup.number().required('יש למלא את שנת הייצור של הרכב'),
    "car_km": Yup.number().required('יש למלא את הקילומטראז\' של הרכב'),
    car_is_hit: Yup.string().required('בלה'),
    "car_test_end": Yup.date().required("יש למלא תאריך טסט"),
    "car_price": Yup.number().required('יש למלא את המחיר המבוקש על הרכב'),
    "payment_date": Yup.date().required("יש למלא תאריך התשלום"),
    "delivery_date": Yup.date().required("יש למלא תאריך קבלת הרכב"),
  })

  const handleNext = (fields, setFieldTouched, errors) => {

    const found = Object.keys(errors).some(errField => fields.includes(errField))

    fields.forEach(field => {
      setFieldTouched(field, true);
    });

    if (!found) {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };


  const getSimilarData = async (values) => {

    const [err, res] = await to(hasuraInstance({
      data: {
        query: QUERY_GET_SIMILAR_CAR_CONTRACTS(values.car_km - 20000, values.car_km + 20000, values.car_brand, values.car_year - 2, values.car_year + 2, values.car_price - 20000, values.car_price + 20000)
      }
    }))

    if (res.data.data.CAR_CONTRACTS) {
      const contractIds = pluck('contract_id', res.data.data.CAR_CONTRACTS)
      const query = GET_CLAUSES_BY_CONTRACT_ID(contractIds);
      const [err, calusesRes] = await to(hasuraInstance({
        data: {
          query: query
        }
      }))

      const orderToJsonArr = map(clause => {return {tag: clause.clauseid.tag, id: clause.clauseid.id, contract_type: clause.clauseid.contract_type, text: clause.clauseid.text}}, calusesRes.data.data.CLAUSES_FOR_SUGGEST_IN_CONTRACT);
      const countOfUses = countBy(prop("id"), orderToJsonArr);
      
      const orderToJsonArrCount = map(obj => {
        return assoc('count', countOfUses[obj.id], obj)
      }, orderToJsonArr)
      
      const SortedJsonArr = sortBy(prop('count'), orderToJsonArrCount)
      return SortedJsonArr;
    }

    return [];
  }

  const [addCarContract, carContractRes] = useMutation(ADD_CAR_CONTRACT);
  const [updateCarContract, updateCarContractRes] = useMutation(UPDATE_CAR_CONTRACT);
  const [addClausesForSuggest, addClausesForSuggestRes] = useMutation(ADD_CLAUSES_FOR_SUGGEST);
  const [updateClausesForSuggest, updateClausesForSuggestRes] = useMutation(UPDATE_CLAUSES_FOR_SUGGEST);
  const [addClausesInContract, addClausesInContractRes] = useMutation(ADD_CLAUSES_IN_CONTRACT);
  const [deleteClausesInContract, deleteClausesInContractRes] = useMutation(DETELE_CLAUSES_FOR_SUGGEST_IN_CONTRACT_BY_ID);
  const [deleteClausesForSuggest, deleteClausesForSuggestRes] = useMutation(DETELE_CLAUSES_FOR_SUGGEST_BY_ID);

  const handleSubmit = async (values) => {
    const { buyer_id,
      buyer_name,
      car_brand,
      car_id,
      car_is_hit,
      car_km,
      car_price,
      car_test_end,
      car_year,
      date_of_sign,
      delivery_date,
      description,
      owner_id,
      owner_name,
      payment_date,
      place_of_sign,
      suggestedClauses,
      additionalClauses,
      title,
      clauses_in_contract } = values;

    const isWaitingForLawyer = (isNil(additionalClauses) || isEmpty(additionalClauses)) ? false : true;
    const status = isWaitingForLawyer ? "מחכה לאישור" : "מאושר";
    const price = isWaitingForLawyer ? 300 : 150;

    if(!isUpdate) {

      const resContract = await (addCarContract({
        variables: {
          buyer_id: buyer_id,
          buyer_name: buyer_name,
          car_brand: car_brand,
          car_id: car_id,
          car_is_hit: car_is_hit,
          car_km: car_km,
          car_price: car_price,
          car_test_end: car_test_end.toISOString(),
          car_year: car_year,
          date_of_sign: date_of_sign.toISOString(),
          delivery_date: delivery_date.toISOString(),
          description: description,
          owner_id: owner_id,
          owner_name: owner_name,
          payment_date: payment_date.toISOString(),
          place_of_sign: place_of_sign,
          title: title,
          user_id: getEmail(),
          status: status,
          price: price
        }
      }))

      const clausesObjects = map((obj) => {
        return { "contract_type": "CAR_RENTAL", "text": obj.text }
      }, additionalClauses ? additionalClauses : [])
  
      const resClausesForSuggest = await (addClausesForSuggest({ variables: { objects: clausesObjects } }))
  
      var clausesInContractObjects = map((obj) => {
        return {
          "contract_id": resContract.data.insert_CAR_CONTRACTS.returning[0].contract_id
          , "clause_id": obj.id
        }
      }, resClausesForSuggest.data.insert_CLAUSES_FOR_SUGGEST.returning
      )
  
      for (let suggest in suggestedClauses) {
        clausesInContractObjects.push({
          "contract_id": resContract.data.insert_CAR_CONTRACTS.returning[0].contract_id
          , "clause_id": suggestedClauses[suggest]
        })
      }
  
      const resClausesInContract = await (addClausesInContract({ variables: { objects: clausesInContractObjects } }))
  
      if (resContract && resClausesForSuggest && resClausesInContract) {
        setSnackbar({ open: true, message: "החוזה נוצר בהצלחה" })
      } else {
        setSnackbar({ open: true, message: "משהו השתבש, נסו מאוחר יותר" })
      }
  
    }
    else {
      const resContract = await (updateCarContract({
        variables: {
          buyer_id: buyer_id,
          buyer_name: buyer_name,
          car_brand: car_brand,
          car_id: car_id,
          car_is_hit: car_is_hit,
          car_km: car_km,
          car_price: car_price,
          car_test_end: car_test_end,
          car_year: car_year,
          date_of_sign: date_of_sign,
          delivery_date: delivery_date,
          description: description,
          owner_id: owner_id,
          owner_name: owner_name,
          payment_date: payment_date,
          place_of_sign: place_of_sign,
          title: title,
          user_id: getEmail(),
          status: status,
          price: price,
          contractId: contract.contract_id,
        }
      }))

      // 1. delete the clasues in contract of the updated contract
      const resClausesInContractDeleted = await (deleteClausesInContract({ variables: { contract_id: contract.contract_id } }))

      let withId = [];
      additionalClauses.forEach(clause => {
        if(clause.id != null) {
          withId.push({ "contract_type": "CAR_RENTAL", "text": clause.text, "id": clause.id })
        }
      });

      // 2. delete the clauses for suggest of the updated contract
      forEach(async (obj) => {
        await deleteClausesForSuggest({ variables: { clause_id: obj.id } });
      }, withId)

      let withoutId = [];
      additionalClauses.forEach(clause => {
        if(clause.id == null) {
          withoutId.push({ "contract_type": "CAR_RENTAL", "text": clause.text })
        }
      });

      // 3. add clauses for suggest (the ones with id are the clauses that were before)
      const resClausesForSuggestWithId = await (addClausesForSuggest({ variables: { objects: withId } }))
      const resClausesForSuggestWithoutId = await (addClausesForSuggest({ variables: { objects: withoutId } }))

      var clausesInContractObjects2 = map((obj) => {
        return {
          "contract_id": contract.contract_id
          , "clause_id": obj.id
        }
      }, resClausesForSuggestWithId.data.insert_CLAUSES_FOR_SUGGEST.returning
      )

      for (let clause in resClausesForSuggestWithoutId.data.insert_CLAUSES_FOR_SUGGEST.returning) {
        clausesInContractObjects2.push({
          "contract_id": contract.contract_id
          , "clause_id": resClausesForSuggestWithoutId.data.insert_CLAUSES_FOR_SUGGEST.returning[clause].id
        })
      }

      for (let suggest in suggestedClauses) {
        clausesInContractObjects2.push({
          "contract_id": contract.contract_id
          , "clause_id": suggestedClauses[suggest]
        })
      }

      // 4. update the clauses in contract (the additional + sugggested)
      const resClausesInContract2 = await (addClausesInContract({ variables: { objects: clausesInContractObjects2 } }))
      
      // let additional = clauses_in_contract.map(clause => { return { id: clause.clauseid.id, text: clause.clauseid.text } })

      // forEach(async (obj) => {
      //   await updateClausesForSuggest({ variables: { clauseId: obj.id, text: obj.text } });
      // }, additional)

      if(resContract){
        setSnackbar({open: true, message: "החוזה עודכן בהצלחה"})
      } else {
        setSnackbar({open: true, message: "משהו השתבש, נסו מאוחר יותר"})
      }
    }
    
    if(getRole() === "lawyer") {
      setTimeout(function(){ history.push(LAWYER_SCREEN); }, 1000);
    }
    else {
      setTimeout(function(){ history.push(MY_CONTRACTS); }, 1000);
    }
  }

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={schema}
        onSubmit={handleSubmit}
      >
        {({ values, setFieldValue, errors, touched, setFieldTouched, dirty, isValid, validateField, validateForm }) => {
          return (
            <Form noValidate>
              <Stepper activeStep={activeStep} alternativeLabel>
                {steps.map((label) => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
              <div>
                {activeStep === steps.length ? (
                  <div>
                    <Typography className={classes.instructions}>All steps completed</Typography>
                    <Button onClick={handleReset}>Reset</Button>
                  </div>
                ) : (
                    <div className={classes.backButton}>
                      <div className={classes.instructions}>{getStepContent(activeStep, handleNext, handleBack, values, setFieldValue, errors, touched, setFieldTouched, dirty, isValid, validateField, validateForm, getSimilarData)}</div>
                    </div>
                  )}
              </div>
            </Form>
          )
        }}
      </Formik>
      <DynamicSnackbar
        open={snackbar.open}
        setOpen={() => setSnackbar({ open: true, message: snackbar.message })}
        message={snackbar.message}
      />
    </>
  );
}

export default withRouter(CarContainer) 