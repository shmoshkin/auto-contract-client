import { Divider, List, ListItem, ListItemIcon, ListItemText, Typography } from '@material-ui/core';
import { DriveEta, House, Lock, People } from '@material-ui/icons';
import { withStyles } from '@material-ui/styles';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { APPARTMENT, CAR, CONFIDENTIALITY, PARTNERSHIP } from '../../constants/route';

const styles = theme => ({
  listContainer: {
    // width: '100%',
    maxWidth: "30%",
    // backgroundColor: "rgb(236, 236, 236)",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "2%",
    textAlign: "center",
  },
  root: {
    marginTop: "5%"
  },
  rootListItem: {
    textAlign: "inherit",
    height: "70px"
  },
  link: {
    cursor: 'pointer',
    color: "black",
    textDecoration: "blink"
  },
  listPad: {
    paddingBottom: "0px"
  }
  
});

class ChooseContractTypeContainer extends Component {

  render() {
    const { classes } = this.props;

        return (
          <div className={classes.root}>
            <Typography variant="h3" align="center">קדימה, נבחר את סוג החוזה</Typography>
            <div className={classes.listContainer}>
            <List component="nav" aria-label="main mailbox folders"
            classes={{
              padding: classes.listPad
            }}>
              <Link to={APPARTMENT} className={classes.link}>
                <ListItem button
                  classes={{
                    root: classes.rootListItem
                  }}>
                  <ListItemIcon>
                    <House fontSize="large"/>
                  </ListItemIcon>
                  <ListItemText primary="השכרת דירה" />
                </ListItem>
              </Link>

              <Divider />
              <Link to={CAR} className={classes.link}>
                <ListItem button
                  classes={{
                    root: classes.rootListItem
                  }}>
                  <ListItemIcon>
                    <DriveEta fontSize="large"/>
                  </ListItemIcon>
                  <ListItemText primary="מכירת/קניית רכב" />
                </ListItem>
              </Link>
              <Divider />
              
              <Link to={PARTNERSHIP} className={classes.link}>
                <ListItem button 
                  classes={{
                    root: classes.rootListItem
                  }}>
                  <ListItemIcon>
                    <People fontSize="large"/>
                  </ListItemIcon>
                  <ListItemText primary="הסכם שותפות" />
                </ListItem>
              </Link>
              <Divider />

              <Link to={CONFIDENTIALITY} className={classes.link}>
                <ListItem button 
                  classes={{
                    root: classes.rootListItem
                  }}>
                  <ListItemIcon>
                    <Lock fontSize="large"/>
                  </ListItemIcon>
                  <ListItemText primary="הסכם סודיות" />
                </ListItem>
              </Link>
              <Divider />
            </List>
            </div>
          </div>
        )
    }
}

export default withStyles(styles)(ChooseContractTypeContainer)