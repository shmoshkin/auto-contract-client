/* eslint-disable */

import { Button, TextField, withStyles } from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import { ErrorMessage, Field } from 'formik';
import React, { useEffect } from 'react';

const styles = theme => ({
    startPageContainer: {
        textAlign: "center",
        marginTop: "2%"
    },
    nameContainer: {
        width: "30%",
        marginLeft: "auto",
        marginRight: "auto",
    },
    descContainer: {
        width: "50%",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "40px",
    },
    inputsContainer: {
        marginTop: "3%"
    },
    title: {
        marginTop: "2%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: '2em',
        marginBottom: '1em',
    },
    errorMsg: {
        fontSize: "10px"
    },
    buttons: {
        textAlign: "center",
        marginTop: "20px"
    }
});

function StartPageContractContainer({classes, handleNext, handleBack, errors, touched, values, dirty, isValid, setFieldValue, setFieldTouched, validateField, validateForm }) {

    useEffect(() => {
        validateForm();
    }, [])

    // const before = (fields) => {
    //     const found = Object.keys(errors).some(errField=> fields.includes(errField))

    //     fields.forEach(field => {
    //       setFieldTouched(field);
    //     });

    //     if(!found) 
    //         handleNext();

    // }
    return (
        <div className={classes.startPageContainer}>
            <FormLabel component="legend" className={classes.title}>כמה פרטים כמה פרטים קטנים להתחלה...</FormLabel>
            <div className={classes.inputsContainer}>
                <div className={classes.nameContainer}>
                {/* <Tooltip title="Add" aria-label="add"> */}
                    <Field
                        required
                        as={TextField}
                        name="title"
                        label="שם החוזה"
                        fullWidth
                        error={touched.title && errors.title}
                    />
                    {/* </Tooltip> */}
                    <ErrorMessage className={classes.errorMsg} name="title" />
                    {/* {touched.title && errors.title && <div>{errors.title}</div>} */}
                </div>
                <div className={classes.descContainer}>
                    <Field 
                        as={TextField}
                        name="description"
                        label="תיאור כללי של החוזה"
                        multiline
                        rows="4"
                        variant="outlined"
                        fullWidth
                    />
                </div>
            </div>
            <div className={classes.buttons}>
                    <Button
                        disabled
                        onClick={handleBack}
                    >
                        הקודם
                    </Button>
                    <Button type="submit" onClick={() => handleNext(["title"], setFieldTouched, errors)} variant="contained" color="primary">
                        המשך
                  </Button>
                </div>
        </div>
    )
}

export default withStyles(styles)(StartPageContractContainer)
