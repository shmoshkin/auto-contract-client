import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/styles';
import to from 'await-to-js';
import React, { Component } from 'react';
import DynamicSnackbar from '../../component/feedback/snackbar';
import Image from '../../component/Login/Image';
import SignIn from '../../component/Login/SignIn';
import SignUp from '../../component/Login/SignUp';
import { HOME } from '../../constants/route';
import { connectWebsocket } from '../../graphql/ws';
import { authInstance } from '../../rest/axios';
import { setCookie } from '../../utils/token';

const styles = theme => ({
  root: {
    height: '100vh',
    width: '100vw'    
  },
  loading: {
    marginRight: '30%'
  }
});

class LoginContainer extends Component {
    
  state = {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      isSignInScreen: true,
      snackbar: {
        open: false,
        message: ""
      },
      loading: false
  }

  onChangeField = (field, value) => this.setState({[field]: value});
  setSnackbarOpen = (value) => this.setState({snackbar:{open: value}})
  
  onSignIn = async () => {

    this.setState({loading: true})
    const [err,res] = await to(authInstance.post(`signIn/${this.state.email}/${this.state.password}`))
    this.setState({loading: false})

    if(err){
      this.setState({snackbar: {open: true ,message: err.response ? err.response.data.message: err.message}})
    } else {
      setCookie(res.data.token)
      this.setState({snackbar: {open: true ,message: res.data.message}})
      connectWebsocket();
      this.props.history.push(HOME);
    }
  }

  onSignUp = async () => {

    this.setState({loading: true})
    const url = `signUp/${this.state.email}/${this.state.password}/${this.state.firstName}/${this.state.lastName}`;
    const [err,res] = await  to(authInstance.post(url))
    this.setState({loading: false})

    if(err){
      this.setState({snackbar: {open: true ,message: "שגיאה בעת התחברות"}})
    } else {
      this.setState({snackbar: {open: true ,message: "הרשמה בוצעה בהצלחה"}})
      this.setScreen()
    }
  }

  setScreen = (isSignIn=true) => this.setState({isSignInScreen:isSignIn})

  render() {
    
    const {classes} = this.props;
    const {isSignInScreen, snackbar, loading} = this.state;

    return (
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Image/>
        {isSignInScreen ?
          <SignIn
            setScreen={this.setScreen}
            onSignIn={this.onSignIn}
            onChangeField={this.onChangeField}
            loading={loading}
          /> :
          <SignUp
            setScreen={this.setScreen}
            onSignUp={this.onSignUp}
            onChangeField={this.onChangeField}
            loading={loading}
          />          
        }       
        <DynamicSnackbar
          open={snackbar.open}
          setOpen={this.setSnackbarOpen}
          message={snackbar.message}
        />
      </Grid>     
    );
  } 
}

export default withStyles(styles)(LoginContainer);
