import { useMutation } from '@apollo/react-hooks';
import Button from '@material-ui/core/Button';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Form, Formik } from 'formik';
import { forEach, isEmpty, isNil, map } from 'ramda';
import React from 'react';
import * as Yup from 'yup';
import AdditonalClauses from '../../component/contract/additionalClauses';
import Step1 from '../../component/contract/confidentiality/Step1';
import Step2 from '../../component/contract/confidentiality/Step2';
import Payment from '../../component/contract/payment';
import SuggestedClauses from '../../component/contract/suggestedClauses';
import DynamicSnackbar from '../../component/feedback/snackbar';
import { LAWYER_SCREEN, MY_CONTRACTS } from '../../constants/route';
import { DETELE_CLAUSES_FOR_SUGGEST_BY_ID, DETELE_CLAUSES_FOR_SUGGEST_IN_CONTRACT_BY_ID } from '../../graphql/queries/clauses';
import { ADD_CLAUSES_FOR_SUGGEST, ADD_CLAUSES_IN_CONTRACT, ADD_SECRET_CONTRACTS, UPDATE_CLAUSES_FOR_SUGGEST, UPDATE_SECRET_CONTRACTS } from '../../graphql/queries/contracts';
import { getEmail, getRole } from '../../utils/token';
import General from '../StartPageContractContainer';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  buttons: {
    position: 'absolute',
    bottom: '2em',
    left: '47%',
  },
  button: {
    marginLeft: '1em',
    marginRight: '1em',
  },
}));

function getSteps() {
  return ['מידע כללי', 'כמה פרטים על חותמי החוזה', 'ועכשיו על ההמצאה', 'הנה כמה הצעות לסעיפים מאיתנו...', 'בא לך להוסיף סעיפים נוספים?', 'תשלום'];
}

function getStepContent(stepIndex, handleNext, handleBack, values, setFieldValue, errors, touched, setFieldTouched, dirty, isValid, validateField, validateForm) {
  switch (stepIndex) {
    case 0:
      return <General handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid} validateField={validateField} validateForm={validateForm}></General>;
    case 1:
      return <Step1 handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></Step1>;
    case 2:
      return <Step2 handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid} ></Step2>;
    case 3:
      return <SuggestedClauses handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}
        type='SECRET_CONTRACT'
      />;
    case 4:
      return <AdditonalClauses handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></AdditonalClauses>;
    case 5:
      return <Payment handleNext={handleNext} handleBack={handleBack} values={values} setFieldValue={setFieldValue} errors={errors} touched={touched} setFieldTouched={setFieldTouched} dirty={dirty} isValid={isValid}></Payment>;
    default:
      return 'Unknown stepIndex';
  }
}

const ConfidentialityContractContainer = ({ history, location: { isUpdate: isUpdate = false, contractId: contractId, contract: contract, isFromLawyer } }) => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(isFromLawyer ? 4 : 0);
  const [snackbar, setSnackbar] = React.useState({ open: false, message: "" });

  const handleUpdateMode = () => {
    let contractCopy = contract;
    let additionalClauses = [];
    let suggestedClauses = {};

    contractCopy.clauses_in_contract.forEach(clause_in_contract => {
      if (clause_in_contract.clauseid.tag === "no tag") {
        additionalClauses.push(clause_in_contract.clauseid);
      }
      else {
        suggestedClauses[clause_in_contract.clauseid.tag] = clause_in_contract.clauseid.id;
      }
    })

    contractCopy.additionalClauses = additionalClauses;
    contractCopy.suggestedClauses = suggestedClauses;
    return contractCopy;
  }

  const [initialValues, setInitialValues] = React.useState(contract ? handleUpdateMode() : {});
  
  // useEffect(() => {
  //   if(contract) {
  //     setInitialValues(contract);
  //   }
  // }, [])

  //   useEffect(() => {
  //     if (isUpdate) {

  //       const getContactData = async () => {
  //         const [err, res] = await to(hasuraInstance({
  //           data: {
  //             query: GET_SECRET_CONTRACT_BY_ID(contractId)
  //           }
  //         }))
  //         if(res.data.data.SECRET_CONTRACTS) {
  //           setInitialValues(res.data.data.SECRET_CONTRACTS[0])
  //         }
  //       }

  //       // const groupByTag = groupBy(clause => clause.clauseid.tag, calusesRes.data.data.CLAUSES_FOR_SUGGEST_IN_CONTRACT)
  //       getContactData();
  //     }
  // }, [])

  const steps = getSteps();

  const schema = Yup.object().shape({
    title: Yup.string().required('יש למלא את שם החוזה'),
    "date_of_sign": Yup.date().required("יש למלא תאריך העסקה"),
    "inventor_name": Yup.string().required('יש למלא את שם הממציא'),
    "inventor_id": Yup.number().required('יש למלא את תז הממציא'),
    "commiter_name": Yup.string().required('יש למלא את שם המתחייב'),
    "commiter_id": Yup.number().required('יש למלא את תז המתחייב'),
    "commiter_city": Yup.string().required('יש למלא את עיר מגורי המתחייב'),
    "invention_details": Yup.string().required('יש למלא את תיאור ההמצאה'),
    "violation_price": Yup.number().required('יש למלא את מחיר הפיצוי המוסכם'),
    "city_of_courts": Yup.string().required('יש למלא את העיר בה יעסקו בתי המשפט'),
  })

  const handleNext = (fields, setFieldTouched, errors) => {

    const found = Object.keys(errors).some(errField => fields.includes(errField))

    fields.forEach(field => {
      setFieldTouched(field, true);
    });

    if (!found) {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const [addSecretContract, addSecretContractRes] = useMutation(ADD_SECRET_CONTRACTS);
  const [updateSecretContract, updateSecretContractRes] = useMutation(UPDATE_SECRET_CONTRACTS);
  const [addClausesForSuggest, addClausesForSuggestRes] = useMutation(ADD_CLAUSES_FOR_SUGGEST);
  const [updateClausesForSuggest, updateClausesForSuggestRes] = useMutation(UPDATE_CLAUSES_FOR_SUGGEST);
  const [addClausesInContract, addClausesInContractRes] = useMutation(ADD_CLAUSES_IN_CONTRACT);
  const [deleteClausesInContract, deleteClausesInContractRes] = useMutation(DETELE_CLAUSES_FOR_SUGGEST_IN_CONTRACT_BY_ID);
  const [deleteClausesForSuggest, deleteClausesForSuggestRes] = useMutation(DETELE_CLAUSES_FOR_SUGGEST_BY_ID);

  const handleSubmit = async (values) => {

    const { city_of_courts,
      commiter_city,
      commiter_id,
      commiter_name,
      date_of_sign,
      description,
      invention_details,
      inventor_id,
      inventor_name,
      title,
      violation_price,
      suggestedClauses,
      additionalClauses,
      clauses_in_contract } = values;

    const isWaitingForLawyer = (isNil(additionalClauses) || isEmpty(additionalClauses)) ? false : true;
    const status = isWaitingForLawyer ? "מחכה לאישור" : "מאושר";
    const price = isWaitingForLawyer ? 500 : 250;

    if (!isUpdate) {
      
      const resContract = await (addSecretContract({
        variables: {
          city_of_courts: city_of_courts,
          commiter_city: commiter_city,
          commiter_id: commiter_id,
          commiter_name: commiter_name,
          date_of_sign: date_of_sign.toISOString(),
          description: description,
          invention_details: invention_details,
          inventor_id: inventor_id,
          inventor_name: inventor_name,
          title: title,
          violation_price: violation_price,
          user_id: getEmail(),
          status: status,
          price: price
        }
      }))

      const clausesObjects = map((obj) => {
        return { "contract_type": "SECRET_CONTRACT", "text": obj.text }
      }, additionalClauses ? additionalClauses : [])

      const resClausesForSuggest = await (addClausesForSuggest({ variables: { objects: clausesObjects } }))

      var clausesInContractObjects = map((obj) => {
        return {
          "contract_id": resContract.data.insert_SECRET_CONTRACTS.returning[0].contract_id
          , "clause_id": obj.id
        }
      }, resClausesForSuggest.data.insert_CLAUSES_FOR_SUGGEST.returning
      )

      for (let suggest in suggestedClauses) {
        clausesInContractObjects.push({
          "contract_id": resContract.data.insert_SECRET_CONTRACTS.returning[0].contract_id
          , "clause_id": suggestedClauses[suggest]
        })
      }

      const resClausesInContract = await (addClausesInContract({ variables: { objects: clausesInContractObjects } }))

      if (resContract && resClausesForSuggest && resClausesInContract) {
        setSnackbar({ open: true, message: "החוזה נוצר בהצלחה" })
      } else {
        setSnackbar({ open: true, message: "משהו השתבש, נסו מאוחר יותר" })
      }

    }
    else {
      const resContract = await (updateSecretContract({
        variables: {
          city_of_courts: city_of_courts,
          commiter_city: commiter_city,
          commiter_id: commiter_id,
          commiter_name: commiter_name,
          date_of_sign: date_of_sign,
          description: description,
          invention_details: invention_details,
          inventor_id: inventor_id,
          inventor_name: inventor_name,
          title: title,
          violation_price: violation_price,
          user_id: getEmail(),
          status: status,
          contractId: contract.contract_id
        }
      }))

      // 1. delete the clasues in contract of the updated contract
      const resClausesInContractDeleted = await (deleteClausesInContract({ variables: { contract_id: contract.contract_id } }))

      let withId = [];
      additionalClauses.forEach(clause => {
        if(clause.id != null) {
          withId.push({ "contract_type": "SECRET_CONTRACT", "text": clause.text, "id": clause.id })
        }
      });

      // 2. delete the clauses for suggest of the updated contract
      forEach(async (obj) => {
        await deleteClausesForSuggest({ variables: { clause_id: obj.id } });
      }, withId)

      let withoutId = [];
      additionalClauses.forEach(clause => {
        if(clause.id == null) {
          withoutId.push({ "contract_type": "SECRET_CONTRACT", "text": clause.text })
        }
      });

      // 3. add clauses for suggest (the ones with id are the clauses that were before)
      const resClausesForSuggestWithId = await (addClausesForSuggest({ variables: { objects: withId } }))
      const resClausesForSuggestWithoutId = await (addClausesForSuggest({ variables: { objects: withoutId } }))

      var clausesInContractObjects2 = map((obj) => {
        return {
          "contract_id": contract.contract_id
          , "clause_id": obj.id
        }
      }, resClausesForSuggestWithId.data.insert_CLAUSES_FOR_SUGGEST.returning
      )

      for (let clause in resClausesForSuggestWithoutId.data.insert_CLAUSES_FOR_SUGGEST.returning) {
        clausesInContractObjects2.push({
          "contract_id": contract.contract_id
          , "clause_id": resClausesForSuggestWithoutId.data.insert_CLAUSES_FOR_SUGGEST.returning[clause].id
        })
      }

      for (let suggest in suggestedClauses) {
        clausesInContractObjects2.push({
          "contract_id": contract.contract_id
          , "clause_id": suggestedClauses[suggest]
        })
      }

      // 4. update the clauses in contract (the additional + sugggested)
      const resClausesInContract2 = await (addClausesInContract({ variables: { objects: clausesInContractObjects2 } }))


      // let a = [];
      
      // for (let clause in additionalClauses)
      // a.push({
      //   "contract_id": contract.contract_id,
      //   "clause_id": clause.id
      // })

      // let additional = clauses_in_contract.map(clause => { return { id: clause.clauseid.id, text: clause.clauseid.text } })

      // forEach(async (obj) => {
      //   await updateClausesForSuggest({ variables: { clauseId: obj.id, text: obj.text } });
      // }, additional)

      if(resContract){
        setSnackbar({open: true, message: "החוזה עודכן בהצלחה"})
      } else {
        setSnackbar({open: true, message: "משהו השתבש, נסו מאוחר יותר"})
      }
    }

    if(getRole() === "lawyer") {
      setTimeout(function(){ history.push(LAWYER_SCREEN); }, 1000);
    }
    else {
      setTimeout(function(){ history.push(MY_CONTRACTS); }, 1000);
    }
  }
  return (
    <div className={classes.root}>
      <Formik
        initialValues={initialValues}
        validationSchema={schema}
        onSubmit={handleSubmit}
      >
        {({ values, setFieldValue, errors, touched, setFieldTouched, dirty, isValid, validateField, validateForm }) => {
          return (
            <Form noValidate>
              <Stepper activeStep={activeStep} alternativeLabel>
                {steps.map((label) => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
              <div>
                {activeStep === steps.length ? (
                  <div>
                    <Typography className={classes.instructions}>All steps completed</Typography>
                    <Button onClick={handleReset}>Reset</Button>
                  </div>
                ) : (
                    <div className={classes.backButton}>
                      <div className={classes.instructions}>{getStepContent(activeStep, handleNext, handleBack, values, setFieldValue, errors, touched, setFieldTouched, dirty, isValid, validateField, validateForm)}</div>
                    </div>
                  )}
              </div>
            </Form>
          )
        }}
      </Formik>
      <DynamicSnackbar
        open={snackbar.open}
        setOpen={() => setSnackbar({ open: true, message: snackbar.message })}
        message={snackbar.message}
      />
    </div>
  );
}

export default ConfidentialityContractContainer;