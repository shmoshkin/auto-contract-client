import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import NotFound from '../../component/general/notFound';
import PrivateRoute from '../../component/route/privateRoute';
import RoleRoute from '../../component/route/roleRoute';
import { APPARTMENT, APP_BAR, CAR, CHOOSE_CONTRACT, CONFIDENTIALITY, GENERAL, HOME, LAWYER_SCREEN, LOGIN, MANAGER, MY_CONTRACTS, PARTNERSHIP } from '../../constants/route';
import ApartmentLeaseContainer from '../ApartmentLeaseContainer';
import AppBarContainer from '../AppBarContainer';
import CarContainer from '../CarContainer';
import ChooseContractTypeContainer from '../ChooseContractTypeContainer';
import ConfidentialityContainer from '../ConfidentialityContainer';
import LawyerContainer from '../LawyerContainer';
import LoginContainer from '../LoginContainer';
import ManagerContainer from '../ManagerContainer';
import MyContractsContainer from '../MyContractsContainer';
import PartnershipContainer from '../PatnershipContainer';
import StartPageContractContainer from '../StartPageContractContainer';
import './index.css';

const App = (props) => {

    return(    
        <>
          <PrivateRoute path={APP_BAR} component={AppBarContainer} />          
          <Switch>
            <Route path={LOGIN} component={LoginContainer} />            
            <RoleRoute path={MANAGER} role="admin" component={ManagerContainer}/>
            <PrivateRoute path={HOME} component={MyContractsContainer} />
            <PrivateRoute path={MY_CONTRACTS} component={MyContractsContainer} />
            <PrivateRoute path={LAWYER_SCREEN} component={LawyerContainer} />
            <PrivateRoute path={CHOOSE_CONTRACT} component={ChooseContractTypeContainer} />
            <PrivateRoute path={GENERAL} component={StartPageContractContainer} />
            <PrivateRoute path={APPARTMENT} component={ApartmentLeaseContainer} />
            <PrivateRoute path={PARTNERSHIP} component={PartnershipContainer} />
            <PrivateRoute path={CONFIDENTIALITY} component={ConfidentialityContainer} />
            <PrivateRoute path={CAR} component={CarContainer} />
            <PrivateRoute component={NotFound} />
          </Switch>
        </>)   
};

export default withRouter(App);