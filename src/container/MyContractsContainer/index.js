import { useQuery } from '@apollo/react-hooks';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/styles';
import { concat, map } from 'ramda';
import React from 'react';
import { withRouter } from 'react-router-dom';
import ContractView from '../../component/contract/contractView';
import Loading from '../../component/feedback/loading';
import { CHOOSE_CONTRACT } from '../../constants/route';
import { QUERY_GET_ALL_CONTRACTS } from '../../graphql/queries/contracts';


const styles = (theme) => ({
  root: {
    flexGrow: 1,
    marginTop: "5%",
    marginRight: "2%",    
    flip:false
  },
  noContract: {
    textAlign: 'center'
  },
  loading: {
    marginTop: '20%',
    marginRight: '50%',
    flip: false
  },
  button: {
    marginTop: '3%',
    width: '15%'
  },
  paper: {
    textAlign: 'center',
  },
});

const MyContractsContainer = ({classes, history}) => {

  const {
    data,
    loading,
    error
  } = useQuery(QUERY_GET_ALL_CONTRACTS);

  if (loading) return (<div className={classes.loading}><Loading/></div>)
  if (error) return <p>error</p>
  
  const allContracts = concat(concat(concat(
    data.APP_RENTAL_CONTRACTS,
    data.CAR_CONTRACTS),
    data.SECRET_CONTRACTS),
    data.PARTNERSHIP_CONTRACTS)  

  return (
    <div className={classes.root} dir="rtl">
      {allContracts.length > 0 ?
          <div className={classes.root}>              
          <Grid container spacing={3}>
            {map(contract => {
                return (
                    <Grid key={contract.contract_id} item xs={4}>
                        <ContractView 
                          contract={contract} 
                          isTranslate={true} 
                          history={history}
                          isEdit={true}
                          isCancel={false}
                          isApprove={false}
                          isComment={true}
                        />
                    </Grid>
                )
            },
            allContracts)}
          </Grid>
        </div> :
        <div className={classes.noContract}>
          <Typography variant="h3" component="h2">
            אופס.. נראה שעוד לא יצרת חוזה
          </Typography>
          <Button onClick={() => history.push(CHOOSE_CONTRACT)} variant="contained" color="primary" className={classes.button}>
            ליצירה לחצ/י כאן
          </Button>
        </div>
        }
    </div>
  )
}

export default withStyles(styles)(withRouter(MyContractsContainer))