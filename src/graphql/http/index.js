import { HttpLink } from 'apollo-link-http';

import {getToken, getRole} from '../../utils/token';

export const getHttpLink = () => (
  new HttpLink({
    uri: process.env.REACT_APP_HASURA_ENDPOINT,
    credentials: 'include'
}))