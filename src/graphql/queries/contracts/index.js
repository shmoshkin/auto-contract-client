import { gql } from 'apollo-boost';

export const QUERY_GET_ALL_CONTRACTS = gql`query get_all_contracts {
  APP_RENTAL_CONTRACTS {
    contract_id
    title
    updated_at
    user_id
    type
    status
    description
    lawyer_description
    app_city
    app_content_details
    app_level
    app_num
    app_rooms_details
    app_rooms_number
    app_street
    arnona
    bail_address
    bail_name
    clauses_in_contract {
      clause_id
      contract_id
      id
      clauseid {
        contract_type
        id
        tag
        text
      }
    }
    date_of_enter
    date_of_leave
    date_of_sign
    electricity
    internet
    is_parking_exists
    is_partners_exists
    is_partners_together
    is_pets_allowed
    is_warehouse_exists
    owner_id
    owner_name
    partners_details
    payment_period_in_months
    place_of_sign
    price_for_month
    renter_id
    renter_name
    vaad
    water
    tv
  }
  CAR_CONTRACTS {
    contract_id
    title
    user_id
    updated_at
    type
    status
    description
    lawyer_description
    buyer_id
    buyer_name
    car_brand
    car_id
    car_is_hit
    car_km
    car_price
    car_test_end
    car_year
    clauses_in_contract {
      clause_id
      clauseid {
        contract_type
        id
        tag
        text
      }
      contract_id
      id
    }
    date_of_sign
    delivery_date
    owner_id
    owner_name
    payment_date
    place_of_sign
  }
  SECRET_CONTRACTS {
    title
    user_id
    updated_at
    type
    status
    description
    lawyer_description
    city_of_courts
    clauses_in_contract {
      clause_id
      clauseid {
        contract_type
        id
        tag
        text
      }
      contract_id
      id
    }
    commiter_city
    commiter_id
    commiter_name
    contract_id
    date_of_sign
    invention_details
    inventor_id
    inventor_name
    violation_price
  }
  PARTNERSHIP_CONTRACTS {
    contract_id
    user_id
    updated_at
    title
    type
    status
    description
    lawyer_description
    a_investment
    a_person_address
    a_person_id
    a_person_job
    a_person_name
    a_person_percents
    b_investment
    b_person_address
    b_person_id
    b_person_job
    b_person_name
    b_person_percents
    clauses_in_contract {
      clause_id
      clauseid {
        contract_type
        id
        tag
        text
      }
      contract_id
      id
    }
    court_for_contract
    date_of_sign
    details_on_calc_books
    end_investment_date
    partnership_goals
    partnership_idea
    partnership_name
    years_of_no_competition
  }
}
`

export const QUERY_GET_ALL_WAITING_TO_APPROVE_CONTRACTS = gql`query get_all_contracts {
  CAR_CONTRACTS(where: {status: {_eq: "מחכה לאישור"}}) {
    contract_id
    title
    user_id
    updated_at
    type
    status
    description
    lawyer_description
    buyer_id
    buyer_name
    car_brand
    car_id
    car_is_hit
    car_km
    car_price
    car_test_end
    car_year
    clauses_in_contract {
      clause_id
      clauseid {
        contract_type
        id
        tag
        text
      }
      contract_id
      id
    }
    date_of_sign
    delivery_date
    owner_id
    owner_name
    payment_date
    place_of_sign
    price
  }
  SECRET_CONTRACTS(where: {status: {_eq: "מחכה לאישור"}}) {
    contract_id
    title
    user_id
    updated_at
    type
    status
    description
    lawyer_description
    city_of_courts
    clauses_in_contract {
      clause_id
      clauseid {
        contract_type
        id
        tag
        text
      }
      contract_id
      id
    }
    commiter_city
    commiter_id
    commiter_name
    date_of_sign
    invention_details
    inventor_id
    inventor_name
    price
    violation_price
  }
  PARTNERSHIP_CONTRACTS(where: {status: {_eq: "מחכה לאישור"}}) {
    contract_id
    user_id
    updated_at
    title
    type
    status
    description
    lawyer_description
    a_investment
    a_person_address
    a_person_id
    a_person_job
    a_person_name
    a_person_percents
    b_investment
    b_person_address
    b_person_id
    b_person_job
    b_person_name
    b_person_percents
    clauses_in_contract {
      clause_id
      clauseid {
        contract_type
        id
        tag
        text
      }
      contract_id
      id
    }
    court_for_contract
    date_of_sign
    details_on_calc_books
    end_investment_date
    partnership_goals
    partnership_idea
    partnership_name
    price
    years_of_no_competition
  }
  APP_RENTAL_CONTRACTS(where: {status: {_eq: "מחכה לאישור"}}) {
    contract_id
    title
    updated_at
    user_id
    type
    status
    description
    lawyer_description
    app_city
    app_content_details
    app_level
    app_num
    app_rooms_details
    app_rooms_number
    app_street
    arnona
    bail_address
    bail_name
    clauses_in_contract {
      clause_id
      clauseid {
        contract_type
        id
        tag
        text
      }
      contract_id
      id
    }
    date_of_enter
    date_of_leave
    date_of_sign
    electricity
    internet
    is_parking_exists
    is_partners_exists
    is_partners_together
    is_pets_allowed
    is_warehouse_exists
    owner_id
    owner_name
    partners_details
    payment_period_in_months
    place_of_sign
    price
    price_for_month
    renter_id
    renter_name
    tv
    vaad
    water
  }
}
`

export const QUERY_GET_SUGGESTED_CLAUSES = gql`query get_suggested_clauses($contractType: String!) {
  CLAUSES_FOR_SUGGEST(where: {contract_type: {_eq: $contractType}, tag: {_neq: "no tag"}}) {
    id
    text
    contract_type
    tag
  }
}`

export const UPDATE_CAR_CONTRACT_STATUS = gql`mutation setContractStatus($contract_id: Int!, $lawyer_description: String!, $status: String!) {
  update_CAR_CONTRACTS(where: {contract_id: {_eq: $contract_id}}, _set: {status: $status, lawyer_description: $lawyer_description}){
    affected_rows
  }
}`

export const UPDATE_APP_CONTRACT_STATUS = gql`mutation setContractStatus($contract_id: Int!, $lawyer_description: String!, $status: String!) {
  update_APP_RENTAL_CONTRACTS(where: {contract_id: {_eq: $contract_id}}, _set: {status: $status, lawyer_description: $lawyer_description}){
    affected_rows
  }
}`

export const UPDATE_PARTNERSHIP_CONTRACT_STATUS = gql`mutation setContractStatus($contract_id: Int!, $lawyer_description: String!, $status: String!) {
  update_PARTNERSHIP_CONTRACTS(where: {contract_id: {_eq: $contract_id}}, _set: {status: $status, lawyer_description: $lawyer_description}){
    affected_rows
  }
}`

export const UPDATE_SECRET_CONTRACT_STATUS = gql`mutation setContractStatus($contract_id: Int!, $lawyer_description: String!, $status: String!) {
  update_SECRET_CONTRACTS(where: {contract_id: {_eq: $contract_id}}, _set: {status: $status, lawyer_description: $lawyer_description}){
    affected_rows
  }
}`

export const ADD_CAR_CONTRACT = gql`mutation insertContract($buyer_id: Int, $buyer_name: String, $car_brand: String, $car_id: Int, $car_is_hit: String, $car_km: Int, $car_price: Int, $car_test_end: date, $car_year: Int, $date_of_sign: date, $delivery_date: date, $description: String, $owner_id: Int, $owner_name: String, $payment_date: date, $place_of_sign: String, $title: String, $user_id: String, $status: String, $price: Int) {
  insert_CAR_CONTRACTS(objects: {buyer_id: $buyer_id, buyer_name: $buyer_name, car_brand: $car_brand, car_id: $car_id, car_is_hit: $car_is_hit, car_km: $car_km, car_price: $car_price, car_test_end: $car_test_end, car_year: $car_year, date_of_sign: $date_of_sign, delivery_date: $delivery_date, description: $description, owner_id: $owner_id, owner_name: $owner_name, payment_date: $payment_date, place_of_sign: $place_of_sign, title: $title, user_id: $user_id, status: $status, price: $price}) {
    affected_rows
    returning {
      contract_id
    }
  }
}
`
export const UPDATE_CAR_CONTRACT =
  gql`mutation updateContract($contractId: Int!, $buyer_id: Int, $buyer_name: String, $car_brand: String, $car_id: Int, $car_is_hit: String, $car_km: Int, $car_price: Int, $car_test_end: date, $car_year: Int, $date_of_sign: date, $delivery_date: date, $description: String, $owner_id: Int, $owner_name: String, $payment_date: date, $place_of_sign: String, $title: String, $user_id: String, $status: String, $price: Int) {
    update_CAR_CONTRACTS(where: {contract_id: {_eq: $contractId}}, _set: {buyer_id: $buyer_id, buyer_name: $buyer_name, car_brand: $car_brand, car_id: $car_id, car_is_hit: $car_is_hit, car_km: $car_km, car_price: $car_price, car_test_end: $car_test_end, car_year: $car_year, date_of_sign: $date_of_sign, delivery_date: $delivery_date, description: $description, owner_id: $owner_id, owner_name: $owner_name, payment_date: $payment_date, place_of_sign: $place_of_sign, title: $title, user_id: $user_id, status: $status, price: $price}) {
      affected_rows
    }
  }
  `

export const ADD_APARTMENT_LEASE_CONTRACT = gql`mutation insertContract($app_city: String, $app_content_details: String, $app_level: Int, $app_num: Int, $app_rooms_details: String, $app_rooms_number: float8, $app_street: String, $arnona: String, $bail_address: String, $bail_name: String, $date_of_enter: date, $date_of_leave: date, $date_of_sign: date, $description: String, $electricity: String, $internet: String, $is_parking_exists: String, $is_partners_exists: String, $is_partners_together: String, $is_pets_allowed: String, $is_warehouse_exists: String, $owner_id: Int, $owner_name: String, $partners_details: String, $payment_period_in_months: Int, $place_of_sign: String, $price_for_month: Int, $renter_id: Int, $renter_name: String, $title: String, $tv: String, $user_id: String, $vaad: String, $water: String, $status: String, $price: Int) {
  insert_APP_RENTAL_CONTRACTS(objects: {app_city: $app_city, app_content_details: $app_content_details, app_level: $app_level, app_num: $app_num, app_rooms_details: $app_rooms_details, app_rooms_number: $app_rooms_number, app_street: $app_street, arnona: $arnona, bail_address: $bail_address, bail_name: $bail_name, date_of_enter: $date_of_enter, date_of_leave: $date_of_leave, date_of_sign: $date_of_sign, description: $description, electricity: $electricity, internet: $internet, is_parking_exists: $is_parking_exists, is_partners_exists: $is_partners_exists, is_partners_together: $is_partners_together, is_pets_allowed: $is_pets_allowed, is_warehouse_exists: $is_warehouse_exists, owner_id: $owner_id, owner_name: $owner_name, partners_details: $partners_details, payment_period_in_months: $payment_period_in_months, place_of_sign: $place_of_sign, price_for_month: $price_for_month, renter_id: $renter_id, renter_name: $renter_name, status: $status, title: $title, tv: $tv, user_id: $user_id, vaad: $vaad, water: $water, price: $price}) {
    affected_rows
    returning {
      contract_id
    }
  }
}
`
export const UPDATE_APARTMENT_LEASE_CONTRACT = gql`mutation updateContract($contractId: Int!, $app_city: String, $app_content_details: String, $app_level: Int, $app_num: Int, $app_rooms_details: String, $app_rooms_number: float8, $app_street: String, $arnona: String, $bail_address: String, $bail_name: String, $date_of_enter: date, $date_of_leave: date, $date_of_sign: date, $description: String, $electricity: String, $internet: String, $is_parking_exists: String, $is_partners_exists: String, $is_partners_together: String, $is_pets_allowed: String, $is_warehouse_exists: String, $owner_id: Int, $owner_name: String, $partners_details: String, $payment_period_in_months: Int, $place_of_sign: String, $price_for_month: Int, $renter_id: Int, $renter_name: String, $title: String, $tv: String, $user_id: String, $vaad: String, $water: String, $status: String, $price: Int) {
  update_APP_RENTAL_CONTRACTS(where: {contract_id: {_eq: $contractId}}, _set: {app_city: $app_city, app_content_details: $app_content_details, app_level: $app_level, app_num: $app_num, app_rooms_details: $app_rooms_details, app_rooms_number: $app_rooms_number, app_street: $app_street, arnona: $arnona, bail_address: $bail_address, bail_name: $bail_name, date_of_enter: $date_of_enter, date_of_leave: $date_of_leave, date_of_sign: $date_of_sign, description: $description, electricity: $electricity, internet: $internet, is_parking_exists: $is_parking_exists, is_partners_exists: $is_partners_exists, is_partners_together: $is_partners_together, is_pets_allowed: $is_pets_allowed, is_warehouse_exists: $is_warehouse_exists, owner_id: $owner_id, owner_name: $owner_name, partners_details: $partners_details, payment_period_in_months: $payment_period_in_months, place_of_sign: $place_of_sign, price_for_month: $price_for_month, renter_id: $renter_id, renter_name: $renter_name, status: $status, title: $title, tv: $tv, user_id: $user_id, vaad: $vaad, water: $water, price: $price}) {
    affected_rows
  }
}
`

export const ADD_PARTNERSHIP_CONTRACT = gql`mutation insertContract($a_investment: Int,$a_person_address: String, $a_person_id: Int, $a_person_job: String, $a_person_name: String, $a_person_percents: Int, $b_investment: Int, $b_person_address: String, $b_person_id: Int, $b_person_job: String, $b_person_name: String, $b_person_percents: Int, $court_for_contract: String, $date_of_sign: date, $description: String, $details_on_calc_books: String, $end_investment_date: date, $partnership_idea: String, $years_of_no_competition: Int, $user_id: String, $title: String, $status: String, $partnership_name: String, $partnership_goals: String, $price: Int) {
  insert_PARTNERSHIP_CONTRACTS(objects: {a_investment: $a_investment, a_person_address: $a_person_address, a_person_id: $a_person_id, a_person_job: $a_person_job, a_person_name: $a_person_name, a_person_percents: $a_person_percents, b_investment: $b_investment, b_person_address: $b_person_address, b_person_id: $b_person_id, b_person_job: $b_person_job, b_person_name: $b_person_name, b_person_percents: $b_person_percents, court_for_contract: $court_for_contract, date_of_sign: $date_of_sign, description: $description, details_on_calc_books: $details_on_calc_books, end_investment_date: $end_investment_date, partnership_idea: $partnership_idea, years_of_no_competition: $years_of_no_competition, user_id: $user_id, title: $title, status: $status, partnership_name: $partnership_name, partnership_goals: $partnership_goals, price: $price}) {
    affected_rows
    returning {
      contract_id
    }
  }
}
`

export const UPDATE_PARTNERSHIP_CONTRACT = gql`mutation updateContract($contractId: Int!, $a_investment: Int,$a_person_address: String, $a_person_id: Int, $a_person_job: String, $a_person_name: String, $a_person_percents: Int, $b_investment: Int, $b_person_address: String, $b_person_id: Int, $b_person_job: String, $b_person_name: String, $b_person_percents: Int, $court_for_contract: String, $date_of_sign: date, $description: String, $details_on_calc_books: String, $end_investment_date: date, $partnership_idea: String, $years_of_no_competition: Int, $user_id: String, $title: String, $status: String, $partnership_name: String, $partnership_goals: String, $price: Int) {
  update_PARTNERSHIP_CONTRACTS(where: {contract_id: {_eq: $contractId}}, _set: {a_investment: $a_investment, a_person_address: $a_person_address, a_person_id: $a_person_id, a_person_job: $a_person_job, a_person_name: $a_person_name, a_person_percents: $a_person_percents, b_investment: $b_investment, b_person_address: $b_person_address, b_person_id: $b_person_id, b_person_job: $b_person_job, b_person_name: $b_person_name, b_person_percents: $b_person_percents, court_for_contract: $court_for_contract, date_of_sign: $date_of_sign, description: $description, details_on_calc_books: $details_on_calc_books, end_investment_date: $end_investment_date, partnership_idea: $partnership_idea, years_of_no_competition: $years_of_no_competition, user_id: $user_id, title: $title, status: $status, partnership_name: $partnership_name, partnership_goals: $partnership_goals, price: $price}) {
    affected_rows
  }
}
`

export const ADD_SECRET_CONTRACTS = gql`mutation insertContract($city_of_courts: String, $commiter_city: String, $commiter_id: Int, $commiter_name: String, $date_of_sign: date, $description: String, $invention_details: String, $inventor_id: Int, $inventor_name: String, $status: String, $title: String, $user_id: String, $violation_price: Int, $price: Int) {
  insert_SECRET_CONTRACTS(objects: {city_of_courts: $city_of_courts, commiter_city: $commiter_city, commiter_id: $commiter_id, commiter_name: $commiter_name, date_of_sign: $date_of_sign, description: $description, invention_details: $invention_details, inventor_id: $inventor_id, inventor_name: $inventor_name, status: $status, title: $title, user_id: $user_id, violation_price: $violation_price, price: $price}) {
    affected_rows
    returning {
      contract_id
    }
  }
}
`
export const UPDATE_SECRET_CONTRACTS =
  gql`mutation updateContract($contractId: Int!, $city_of_courts: String, $commiter_city: String, $commiter_id: Int, $commiter_name: String, $date_of_sign: date, $description: String, $invention_details: String, $inventor_id: Int, $inventor_name: String, $status: String, $title: String, $user_id: String, $violation_price: Int) {
    update_SECRET_CONTRACTS(where: {contract_id: {_eq: $contractId}}, _set: {city_of_courts: $city_of_courts, commiter_city: $commiter_city, commiter_id: $commiter_id, commiter_name: $commiter_name, date_of_sign: $date_of_sign, description: $description, invention_details: $invention_details, inventor_id: $inventor_id, inventor_name: $inventor_name, status: $status, title: $title, user_id: $user_id, violation_price: $violation_price}) {
      affected_rows
    }
  }
  `

export const ADD_CLAUSES_FOR_SUGGEST = gql`
mutation insert_multiple_articles($objects: [CLAUSES_FOR_SUGGEST_insert_input!]!) {
  insert_CLAUSES_FOR_SUGGEST(objects: $objects) {
    affected_rows
    returning {
      id
    }
  }
}
`

export const UPDATE_CLAUSES_FOR_SUGGEST = gql`
mutation update_clauses_for_suggest($clauseId: Int, $text:String) {
  update_CLAUSES_FOR_SUGGEST(where: {id: {_eq: $clauseId}}, _set: {text: $text}) {
    affected_rows
  }
}
`

export const ADD_CLAUSES_IN_CONTRACT = gql`
mutation MyMutation($objects: [CLAUSES_FOR_SUGGEST_IN_CONTRACT_insert_input!]!) {
  insert_CLAUSES_FOR_SUGGEST_IN_CONTRACT(objects: $objects) {
    affected_rows
  }
}
`

export const QUERY_GET_SIMILAR_CAR_CONTRACTS = (minKM, maxKM, brand, minYear, maxYear, minPrice, maxPrice) => {
  return `query get_similar_car_contracts {
    CAR_CONTRACTS(where: {_and: {car_brand: {_eq: "${brand}"}, car_km: {_gte: ${minKM}, _lte: ${maxKM}}, car_price: {_gte: ${minPrice}, _lte: ${maxPrice}}, car_year: {_gte: ${minYear}, _lte: ${maxYear}}}}) {
      contract_id   
    }
  }`}

export const QUERY_GET_SIMILAR_APP_CONTRACTS = (city, street) => {
  return `query get_similar_app_contracts {
    APP_RENTAL_CONTRACTS(where: {app_city: {_eq: "${city}"}, app_street: {_eq: "${street}"}}) {
      contract_id
    }
  }
  `}


export const QUERY_GET_SIMILAR_CAR_CONTRACTS_DEMO = (minKM, maxKM, brand, minYear, maxYear, minPrice, maxPrice) => {
  return `query get_similar_car_contracts {
    CAR_CONTRACTS {
      contract_id   
    }
  }`}

export const GET_CLAUSES_BY_CONTRACT_ID = (contractIdArr) => {
  return `query MyQuery {
    CLAUSES_FOR_SUGGEST_IN_CONTRACT(where: {contract_id: {_in: [${contractIdArr}]}, _and: {clauseid: {tag: {_neq: "no tag"}}}}) {
      contract_id
      clauseid {
        text
        id
        tag
        contract_type
      }
    }
  }`
}

export const GET_SECRET_CONTRACT_BY_ID = (contractId) => {
  return ` query get_secret_contract_by_id {
    SECRET_CONTRACTS(where: {contract_id: {_eq: ${contractId}}}) {
      city_of_courts
      clauses_in_contract {
        clauseid {
          contract_type
          id
          tag
          text
        }
        clause_id
        contract_id
        id
      }
      commiter_city
      commiter_id
      commiter_name
      contract_id
      date_of_sign
      description
      invention_details
      inventor_id
      inventor_name
      lawyer_description
      status
      title
      type
      updated_at
      user_id
      violation_price
    }
  }
  `
}