import { gql } from 'apollo-boost';

export const GET_ALL_USERS = gql`query get_all_users {
    USERS {      
      created_at
      email
      firstName
      lastName
      role
      updated_at
    }
  }`

export const DELETE_USER_BY_EMAIL = gql `mutation update_user($email: String!) {
  delete_USERS(where: {email: {_eq: $email}}) {
    affected_rows
  }
}`

export const UPDATE_USER_BY_EMAIL = gql`mutation update_user($email: String!, $role: String!) {
  update_USERS(where: {email: {_eq: $email}}, _set: {role: $role}) {
    affected_rows
  }
}`

export const SUBSCRIPTION_GET_ALL_USERS = gql `subscription get_all_users {
  USERS {
    email
    firstName
    lastName
    role
    created_at
  }
}`