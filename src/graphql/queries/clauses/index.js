import { gql } from 'apollo-boost';


export const SUBSCRIPTION_ALL_CLAUSES_FOR_SUGGEST = gql `subscription MyQuery {
    CLAUSES_FOR_SUGGEST {
      contract_type
      tag
      id
      text
    }
  }`

  export const DELETE_CLAUSES_FOR_SUGGEST_BY_ID = gql `mutation deleteClausesById($clause_id: Int!) {
    delete_CLAUSES_FOR_SUGGEST(where: {id: {_eq: $clause_id}}) {
      affected_rows
    }
  }
  `

  export const ADD_CLAUSES_FOR_SUGGEST = gql `mutation addClause($contract_type:String, $tag: String, $text:String) {
    insert_CLAUSES_FOR_SUGGEST(objects: {contract_type: $contract_type, tag: $tag, text: $text}) {
      affected_rows
    }
  }
  
  `

  export const UPDATE_CLAUSES_FOR_SUGGEST_BY_ID = gql`mutation MySubscription($clause_id: Int, $tag: String, $text: String) {
    update_CLAUSES_FOR_SUGGEST(where: {id: {_eq: $clause_id}}, _set: {tag: $tag, text: $text}) {
      affected_rows
    }
  }`

  export const DETELE_CLAUSES_FOR_SUGGEST_IN_CONTRACT_BY_ID = gql `mutation MyMutation($contract_id: Int) {
    delete_CLAUSES_FOR_SUGGEST_IN_CONTRACT(where: {contract_id: {_eq: $contract_id}}) {
      affected_rows
    }
  }
  `

  export const DETELE_CLAUSES_FOR_SUGGEST_BY_ID = gql `mutation MyMutation($clause_id: Int) {
    delete_CLAUSES_FOR_SUGGEST(where: {id: {_eq: $clause_id}}) {
      affected_rows
    }
  }
  `