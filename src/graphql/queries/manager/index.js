import { gql } from 'apollo-boost';


export const PRICES_SUM_OF_CONTRACTS = gql `query MyQuery {
    APP_RENTAL_CONTRACTS_aggregate {
      aggregate {
        sum {
          price
        }
      }
    }
    CAR_CONTRACTS_aggregate {
      aggregate {
        sum {
          price
        }
      }
    }
    SECRET_CONTRACTS_aggregate {
      aggregate {
        sum {
          price
        }
      }
    }
    PARTNERSHIP_CONTRACTS_aggregate {
      aggregate {
        sum {
          price
        }
      }
    }
  }`