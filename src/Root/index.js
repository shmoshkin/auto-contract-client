/* eslint-disable */

import { ApolloProvider } from '@apollo/react-hooks';
import { InMemoryCache } from 'apollo-cache-inmemory';
import ApolloClient from "apollo-client";
import { ApolloLink, split } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { RetryLink } from "apollo-link-retry";
import { getMainDefinition } from 'apollo-utilities';
import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { HOME, LOGIN } from '../constants/route';
import App from '../container/App';
import { getHttpLink } from '../graphql/http';
import { getWsLinkInstance } from '../graphql/ws';
import { clearToken, getRole, getToken } from '../utils/token';




var isRefreshing = false;

const Root = (props) => {
  
  const authLink = setContext((_, { headers }) => {
      return {
        headers: {
          ...headers,
          Authorization: getToken() ? `Bearer ${getToken()}` : "",
          "x-hasura-role": getRole() ? getRole() : 'anonymus'
        }
      }
  });
  
  const wsLink = getWsLinkInstance();
  const httpLink = getHttpLink();
  const authHttpLink = authLink.concat(httpLink)
  
  const link = split(
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query);
      return kind === 'OperationDefinition' && operation === 'subscription';
    },
    wsLink,
    authHttpLink
  );
  
  const client = new ApolloClient({
    link: ApolloLink.from([     
    new RetryLink(),
    link
    ]),
    cache: new InMemoryCache()
  });

  useEffect(() => {
    if (getToken()) {
      if(props.location.pathname === LOGIN ||
        props.location.pathname === '/') props.history.push(HOME);

    } else if(props.location.pathname !== LOGIN) {
      clearToken();
    }
  }, []);


  return (
    <ApolloProvider client={client}>
        <App client={client}/>
    </ApolloProvider>
  )
};

export default withRouter(Root);